<?php
    /* Template Name: Home Page */ 

    get_header(); 
    $template_url = get_bloginfo('template_url');

    //$lang = strtolower($_COOKIE['lang']); //"en";
    $lang = isset($_COOKIE['lang']) ? strtolower($_COOKIE['lang']) : 'en';
    $heading = "wpcf-heading";
    $subHeading = "wpcf-sub-heading";
    $featureContent = "wpcf-sfeatured-content";


    // Get In Touch Fields
    $background = "wpcf-set-background-image";
    $link = "wpcf-link-to-page";
    $gitHeading = "wpcf-git-heading";
    $gitSubHeading = "wpcf-git-sub-heading";
    $addheading = "wpcf-additional-heading";

    global $post; 
?>
<?php while ( have_posts() ): the_post(); ?>

<section class="panel">
    <div id="div_video" class="position-relative">
        <div id="playButton" class="vHMiddle" onclick="playVideoButton()">
            <img class="playButtonImage" src="<?php echo $template_url?>/images/PlayButton.svg">    
        </div>

        <video id="videoPlay" preload="none" class="embed-responsive" playsinline="playsinline" muted="muted" loop="loop" poster="<?php echo do_shortcode('[types field="add-poster-for-video" output="raw"]'); ?>" onclick="playVideo(this)">
            <source src="<?php echo $template_url?>/images/play.mp4" type="video/mp4">
            <!-- <source src="<?php echo do_shortcode('[types field="add-video-file" output="raw"]'); ?>" type="video/mp4"> -->
        </video>
    </div>
</section>

<?php
    $loop = new WP_Query(
        array(
            'post_type' => 'page',
            'order' => 'ASC',
            'tag' => 'featured',
            'posts_per_page' => 3
        )
    );

    if ( $loop->have_posts() ) : 

    while ($loop->have_posts()) : $loop->the_post(); 
    $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'custom-thumb', false, '' );          

    if ( has_post_thumbnail() ) { 
        echo '<section class="details home overlapPanel panel" style="background-image: linear-gradient(rgba(34, 34, 34, 0.8),rgba(34, 34, 34, 0.8)),url('. $src[0].')">';
    } else {
        echo '<section class="details home overlapPanel panel" style="background-image: linear-gradient(rgba(34, 34, 34, 0.6),rgba(34, 34, 34, 0.6)),url('. $template_url.'/images/imageNotAvailable.png);">';
    }
?>
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">
            <div class="col">
                <a href='<?php the_permalink(); ?>'>
                    <?php 
                        if ( get_post_meta($post->ID, $heading."-".$lang, true)) {
                            echo  "<h3 class='text-capitalize'>" . do_shortcode('[types field="'.ltrim($heading,"wpcf-")."-".$lang.'"]');

                            if ( get_post_meta($post->ID, $subHeading."-".$lang, true)) { 
                                echo "<span>" . do_shortcode('[types field="'.ltrim($subHeading,"wpcf-")."-".$lang.'"]') . "</span>";
                            }

                            echo "</h3>";
                        }
                    ?>
                </a>

                <?php 
                    if ( get_post_meta($post->ID, $featureContent."-".$lang, true)) {
                        echo  "<p>" . do_shortcode('[types field="'.ltrim($featureContent,"wpcf-")."-".$lang.'" output="raw"]') . "</p>";
                    }
                ?>

                <h2><a href="<?php the_permalink(); ?>">tell me more »</a></h2>
            </div>
        </div>
    </div>
<?php
    echo '</section>';    
    endwhile; wp_reset_query(); 
    endif;
?>

<?php
    $loop = new WP_Query( 'page_id=14' );

    if ( $loop->have_posts() ) : 

        while ($loop->have_posts()) : $loop->the_post();     
?>
<section id="getInTouch" style="background-image: linear-gradient(rgba(34, 34, 34, 0.7),rgba(34, 34, 34, 0.7)),url(<?php if ( get_post_meta($post->ID, $background, true)) { echo do_shortcode('[types field="set-background-image" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container">
        <div class="row align-items-center justify-content-center">    
            <div class="p-0" data-2000='transform: translate(0, 0px);' data-2600='opacity: 2;'>  <!-- data-3000='transform: translate(0, -100px);' -->
                <a href="<?php if ( get_post_meta($post->ID, $link, true)) { echo do_shortcode('[types field="link-to-page" output="raw"]'); } else { echo "#"; } ?>">
                    <h3>
                        <?php
                            if ( get_post_meta($post->ID, $gitHeading."-".$lang, true)) echo do_shortcode('[types field="'.ltrim($gitHeading,"wpcf-")."-".$lang.'"]');
                            if ( get_post_meta($post->ID, $gitSubHeading."-".$lang, true)) echo "<span>" . do_shortcode('[types field="'.ltrim($gitSubHeading,"wpcf-")."-".$lang.'"]') . "</span>" ;
                        ?>
                    </h3>

                    <h2>
                        <?php
                            if ( get_post_meta($post->ID, $addheading."-".$lang, true)) echo  do_shortcode('[types field="'.ltrim($addheading,"wpcf-")."-".$lang.'"]');
                        ?> &raquo;
                    </h2>
                </a>
            </div>
        </div>
    </div>
</section>
<?php 
        endwhile; wp_reset_query(); 
    endif;
?>

<?php endwhile; wp_reset_query(); ?>

<script>
    function playVideo (scope){ 
        console.log("AAA");
        var flag = $("#playButton").hasClass('videoPlaying') ? true : false;
        if(!flag){       
        $("#playButton").css('display','none');
        $("#playButton").addClass('videoPlaying');
        scope.play();
        }else{
            $("#playButton").css('display','block');
            $("#playButton").removeClass('videoPlaying');
            scope.pause();
        }
    }

    function playVideoButton (){
        console.log("AAAB");
        var flag = $("#playButton").hasClass('videoPlaying') ? true : false;
        if(!flag){       
            $("#playButton").css('display','none');
            $("#playButton").addClass('videoPlaying');
            $("#videoPlay")[0].play();
        }else{
            $("#playButton").css('display','block');
            $("#playButton").removeClass('videoPlaying');
            $("#videoPlay")[0].pause();
        }
       
    }

    var width = (window.innerWidth > 0) ? window.innerWidth : document.documentElement.clientWidth;

    if(width > 1024){

        $(function() { // wait for document ready
            // init
            var flag = false;
            var controller = new ScrollMagic.Controller({
                globalSceneOptions: {
                    triggerHook: 'onLeave',
                    duration: "0%"
                }
            });

            // get all slides
            var slides = document.querySelectorAll("section.panel");
            console.log(slides.length)
            // create scene for every slide
            for (var i = 0; i < slides.length; i++) {
                new ScrollMagic.Scene({
                        triggerElement: slides[i]
                    })
                    .setPin(slides[i], {
                        pushFollowers: false
                    })
                    .addIndicators() // add indicators (requires plugin)
                    .addTo(controller)
            }
            $(window).scroll(function(event) {
                var scroll = $(window).scrollTop();
                if (scroll > 3000) {
                    controller.enabled(false);
                    flag = true;
                } else if (scroll < 3000 && flag) {
                    controller = new ScrollMagic.Controller({
                        globalSceneOptions: {
                            triggerHook: 'onLeave',
                            duration: "0%"
                        }
                    });
                    var slides = document.querySelectorAll("section.panel");
                    for (var i = 0; i < slides.length; i++) {
                        new ScrollMagic.Scene({
                                triggerElement: slides[i]
                            })
                            .setPin(slides[i], {
                                pushFollowers: false
                            })
                            .addIndicators() // add indicators (requires plugin)
                            .addTo(controller)
                    }
                    flag=false;
                }

            });
        });

    }
   
</script>

<?php get_footer(); ?>