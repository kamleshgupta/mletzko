@import url("./fonts/fonts.css");

/* Reset Css */
/* http://meyerweb.com/eric/tools/css/reset/ 
   v2.0 | 20110126
   License: none (public domain)
*/

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}

/* Common Styling Start */
a{
	color: #222222;
}
a:hover{
	text-decoration: none;
	color: #0056b3;
}
a:focus{
	outline: none;
}

@media (min-width: 1200px) {
  .container {
	max-width: 90%;
  }
}

.vHMiddle{
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);	
}


/* Common Styling End */

/* Header Start */
/* The Overlay (background) */
body {
	font-family: 'HelveticaNeue';
	font-size: 16px;
	line-height: 1;
	color: #222222;
}
  
.overlay {
	height: 0%;
	width: 100%;
	position: fixed;
	z-index: 1;
	top: 0;
	left: 0;
	background-color: rgb(0,0,0);
	background-color: rgba(0,0,0, 0.9);
	overflow-x: hidden;
	/* overflow-y: hidden; */
	transition: 0.5s;
}
.overlayCar{
	background: rgba(0, 0, 0, 0.73) center / cover no-repeat;
}
.overlay-content {
	/* position: relative; */
	position: absolute;
	top: 30%;
	width: 100%;
	text-align: center;
	margin-top: 0px;
}

.overlay a {		
	padding: 8px;	
	text-decoration: none;		
	font-size: 29px;
	letter-spacing: 1.16px;
	line-height: 34px;		
	color: #E6E6E6;		
	transition: 0.3s;		
}

.overlay a:hover, .overlay a:focus {
	color: #f1f1f1;
}

.overlay .closebtn {
	margin: 9px -2px;
    display: inline-block;
}

@media screen and (max-height: 450px) {
	.overlay {overflow-y: auto;}
	.overlay a {font-size: 20px}
	.overlay .closebtn {
		font-size: 40px;
		top: 15px;
		right: 35px;
	}
}

.overlay #menuPopupLogo{
	width: 273px;
	height: 69px;
	
}
.overlay #menuPopupItemList h3{
	padding-bottom: 40px;
}
  
.overlay #menuPopupItemList h2:hover, .overlay #menuPopupItemList h2:focus {
	color: #f1f1f1;
}

.overlay #menuPopupItemList h2{
	font: Bold 24px/29px Work Sans;
	letter-spacing: 1.16px;
	color: #818181;
	text-transform: uppercase;
	opacity: 1;
}

.overlay #menuPopupItemList li{
	padding-right: 90px;
}
.overlay #menuPopupItemList h3{
	text-align: center;
	font: Italic 15px/24px Mate;
	letter-spacing: 0;
	color: #E6E6E6;
	opacity: 1;
}

.overlay ul{
	padding-top: 14%;
}

.overlay #menuPopupLogo{
	display: block;
}

@media (max-width: 1024px) {
	.overlay{
		color: #f1f1f1;
	}

	.overlay .closebtn {
		font-size: 40px;
		top: 15px;
		right: 35px;
	}
	/* .overlay-content{
		margin-top: -50px;
	} */
	.overlay .list-inline{
		margin-left: 65px !important;
		margin-top: -110px;
	}
	/*.overlay #menuPopupLogo{
	 	padding-bottom: 30px;
	}*/
	.overlay #menuPopupLogo{
		display: unset;
	}

	.overlay .closebtn{
		z-index: 999999;
	}
}


  @media screen and (max-width: 992px) {
  .overlay a {			
	text-decoration: none;		
	font-size: 30px;
	letter-spacing: 0.72px;
	line-height: 35px;
	}
  }


  
header{
	z-index: 999999;
	top: 25px;
}

/*header #menuPopupLogo{
	top: 431px;
	left: 144px;
	width: 273px;
	height: 69px;
}

.overlay .list-inline{
	padding: 8px;
	text-decoration: none;
	font-size: 36px;
	color: #818181;
	display: block;
	transition: 0.3s;
  }
  */
header .container .col-lg-6{
	width: 50%;
}
#menu-primary-menu li:first-child {
	margin: 0;
  }
  
#menu-primary-menu li{
	float: left;
	margin-left: 130px;
	text-transform: uppercase;
	font-family: 'Work Sans Bold';
	font-size: 29px;
	line-height: 34px;
	letter-spacing: 1.16px;
	color:#E6E6E6;
}

#menu-primary-menu li span{
	display: block;
	font-family: 'Mate Italic';
	text-transform: lowercase;
	font-size: 20px;
	line-height: 24px;
	letter-spacing: 0;
	color:#E6E6E6;
}

.overlay ul.hamburger{
	margin: 0;
    padding: 8px 8px 0 0;
}

.hamburger li{
	display: inline-block;
    vertical-align: middle;
}

.hamburger li:first-child{
	margin-top: -5px;
    padding-right: 20px;
}

.hamburger li a{
	display: inline-block;
    margin: 0;
    padding: 0;
}

#languageSelector{
	font-size: 14px;
	line-height: 16px;
	letter-spacing: 0.84px;
	font-family: 'Work Sans Bold';
}

/* Header End */


/** Below css is for Index page - added by Aadarsh Jain **/
/** for index page video **/
#div-video {
  position: relative;
  background-color: black;
  height: 75vh;
  min-height: 25rem;
  width: 100%;
  overflow: hidden;
}

#div-video video {
  position: absolute;
  top: 50%;
  left: 50%;
  min-width: 100%;
  min-height: 100%;
  width: auto;
  height: auto;
  z-index: 0;
  -ms-transform: translateX(-50%) translateY(-50%);
  -moz-transform: translateX(-50%) translateY(-50%);
  -webkit-transform: translateX(-50%) translateY(-50%);
  transform: translateX(-50%) translateY(-50%);
}

#div-video .container {
  position: relative;
  z-index: 2;
}

#div-video .overlay {
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  background-color: black;
  opacity: 0.5;
  z-index: 1;
}

@media (pointer: coarse) and (hover: none) {
  #div-video {
    background: url('https://source.unsplash.com/XT5OInaElMw/1600x900') black no-repeat center center scroll;
  }
  #div-video video {
    display: none;
  }
}
/** for index page video **/

/** for slider **/

#slider{
	background: url(../images/logoBigTransparency.svg) no-repeat 100%;
	background-size: 100% auto;
	background-position: center;
	padding-top: 130px;
}
#slider h3{
	font-family: 'Mate Italic';
	color: #E6E6E6;
	font-size: 32px;
	line-height: 39px;
	padding-bottom: 1.1rem;
}
.bxslider h4{
	text-transform: uppercase;
	font-family: 'Work Sans Bold';
	font-size: 44px;
	line-height: 52px;
	letter-spacing: 2.64px;
	padding-bottom: 2.8rem;
}
.bxslider p{
	font-size: 22px;
	line-height: 55px;
	font-family: 'Work Sans Light';
	padding: 0 0 5em;
	width: 61%;
	display: inline-block;
}

#loveContent{
	background: url(../images/logoBigTransparency.svg) no-repeat 100%;
	background-size: 100% auto;
	background-position: center;
	padding-top: 130px;
}
.contentDetail h3{
	padding-bottom: 5px;
	font-family: 'Mate Italic';
	color: #E6E6E6;
	font-size: 32px;
	line-height: 39px;
}
.contentDetail h4{
	text-transform: uppercase;
	font-family: 'Work Sans Bold';
	font-size: 44px;
	line-height: 52px;
	letter-spacing: 2.64px;
	padding-bottom: 25px;
}
.contentDetail p{
	font-size: 20px;
	line-height: 55px;
	font-family: 'Work Sans Light';
	padding: 0 0 5em;
}


@media (max-width: 1024px) {
	.bxslider p{
		padding: 0;
	}
	.bx-prev{
		margin-left: -25px;
	}
	.bx-next{
		margin-right: -25px;
	}
	/* .bx-wrapper {
  		padding: 2em 0;
  	} */

  	#slider{
		padding-top: 15px;
	}
	
}
/** for slider **/



#pageCard ul{
	list-style: none;
	padding: 0;
	margin: 0 0 10em;
	width: 100%;
}

#pageCard ul li{
	height: 406px;
	width: 49%;
	float: left;
	text-align: center;
	position: relative;
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
	margin-bottom: 27px;
	/* filter: grayscale(100%) */
}




#pageCard ul li:last-child{
	float: right;
}


#pageCard h2{
	font-family: 'Work Sans Bold';
	text-transform: uppercase;
	font-size: 29px;
	line-height: 34px;
	letter-spacing: 1.16px;
	color: #E6E6E6;
}

#pageCard h2 span{
	display: block;
	font-family: 'Mate Italic';
	text-transform: lowercase;
	font-size: 20px;
	line-height: 24px;
	letter-spacing: 0;
}

@media (max-width: 1024px) {
	#pageCard{
		padding-left: 25px;
		padding-right: 25px;
	}
	#pageCard ul li{
		height: 260px;
		width: 100%;
		float: none;
		text-align: center;
		position: relative;
		background: url(../images/mletzko_09_dreiviertel_front_018.png) no-repeat 100%;
		background-position: center;
		/* filter: grayscale(100%) */
	}

	#pageCard ul{
		list-style: none;
		padding: 0;
		margin: 0 0 0;
	}

	#pageCard h2{
		font-family: 'Work Sans Bold';
		text-transform: uppercase;
		font-size: 24px;
		line-height: 29px;
		letter-spacing: 0.8px;
		color: #E6E6E6;
	}

	#pageCard h2 span{
		display: block;
		font-family: 'Mate Italic';
		text-transform: lowercase;
		font-size: 15px;
		line-height: 19px;
		letter-spacing: 0;
	}
}


#getInTouch{
	background: rgba(0, 0, 0, 0.55) center / cover no-repeat;
	text-align: center;
	color: #E6E6E6;
}

#getInTouch .row{ 
	height: 940px;
}

#getInTouch h3{
	font-family: 'Mate Italic';
	text-transform: lowercase;
	font-size: 32px;
	line-height: 39px;
}

#getInTouch h3 span{
	display: block;
	font-family: 'HelveticaNeue Bold';
	text-transform: uppercase;
	font-size: 42px;
	line-height: 49px;
	margin-bottom: 1em;
}

#getInTouch h2{
	font-family: 'HelveticaNeue Bold';
	text-transform: uppercase;
	font-size: 24px;
	line-height: 28px;
}

#getInTouch a{
	color: #E6E6E6;
}


/* Love Details Start - Soumik */



#detailsOpacity{
	background: rgba(0, 0, 0, 0.8) center / cover no-repeat;
	text-align: center;
	color: #E6E6E6;
    height: 100vh;
}

 #heartbeat{
	top: 0;
    width: 100%;
    background: rgba(0, 0, 0, 0.8);
	position: absolute;
	text-align: center;
	color: #E6E6E6;
	height: 100vh;
} 

#heartbeat .row{ 
	height: 940px;
}

#heartbeat h3{
	font-family: 'Mate Italic';
	/* text-transform: lowercase; */
	font-size: 47px;
	line-height: 57px;
}

#heartbeat h4{
	display: block;
	font-family: 'HelveticaNeue Bold';
	text-transform: uppercase;
	font-size: 63px;
	line-height: 74px;
	letter-spacing: 2.77px;
	margin-left: 10%;
    margin-top: 1%;
}

#heartbeat h2{
	font-family: 'HelveticaNeue Light';
	text-transform: lowercase;
	font-size: 20px;
	line-height: 55px;
	letter-spacing: 1.2px;
	margin-left: 6%;
	opacity: 1;
}

#heartbeat a{
	color: #E6E6E6;
}

#fillerFlap{
	background: rgba(0, 0, 0, 0.55) center / cover no-repeat ;
	text-align: center;
	color: #E6E6E6;
}

#fillerFlap .row{ 
	height: 940px;
}

#fillerFlap h3{
	font-family: 'Mate Italic';
	text-transform: lowercase;
	font-size: 43px;
	line-height: 52px;
	padding-right: 7%;
}

#fillerFlap h4{
	display: block;
	font-family: 'HelveticaNeue Bold';
	text-transform: uppercase;
	font-size: 63px;
	line-height: 74px;
	margin-bottom: 1em;
	letter-spacing: 2.77px;
}

#fillerFlap h2{
	font-family: 'HelveticaNeue Light';
	font-size: 20px;
	line-height: 21px;
}

#fillerFlap a{
	color: #E6E6E6;
}

#outsideMirror{
	background: rgba(0, 0, 0, 0.55) center / cover no-repeat ;
	text-align: center;
	color: #E6E6E6;
}

#outsideMirror .row{ 
	height: 940px;
}

#outsideMirror h3{
	font-family: 'Mate Italic';
	text-transform: lowercase;
	font-size: 43px;
	line-height: 52px;
}

#outsideMirror h3 span{
	display: block;
	font-family: 'HelveticaNeue Bold';
	text-transform: uppercase;
	font-size: 63px;
	line-height: 74px;
	margin-bottom: 1em;
	letter-spacing: 2.77px;
}

#outsideMirror h2{
	font-family: 'HelveticaNeue Light';
	font-size: 20px;
	line-height: 21px;
}

#outsideMirror a{
	color: #E6E6E6;
}

#headlights{
	background: url(../images/mletzko_08_details_02_036.png) rgba(0, 0, 0, 0.55) center / cover no-repeat;
	text-align: center;
	color: #E6E6E6;
}

#headlights .row{ 
	height: 940px;
}

#headlights h3{
	font-family: 'Mate Italic';
	text-transform: lowercase;
	font-size: 43px;
	line-height: 52px;
	padding-right: 7%;
}

#headlights h4{
	display: block;
	font-family: 'HelveticaNeue Bold';
	text-transform: uppercase;
	font-size: 63px;
	line-height: 74px;
	margin-bottom: 1em;
	letter-spacing: 2.77px;
}

#headlights h2{
	font-family: 'HelveticaNeue Light';
	font-size: 20px;
	line-height: 21px;
}

#headlights a{
	color: #E6E6E6;
}

#handles{
	background: url(../images/mletzko_08_details_02_022.png) rgba(0, 0, 0, 0.55) center / cover no-repeat;
	text-align: center;
	color: #E6E6E6;
}

#handles .row{ 
	height: 940px;
}

#handles h3{
	font-family: 'Mate Italic';
	text-transform: lowercase;
	font-size: 43px;
	line-height: 52px;
}

#handles h3 span{
	display: block;
	font-family: 'HelveticaNeue Bold';
	text-transform: uppercase;
	font-size: 63px;
	line-height: 74px;
	margin-bottom: 1em;
	letter-spacing: 2.77px;
}

#handles h2{
	font-family: 'HelveticaNeue Light';
	font-size: 20px;
	line-height: 21px;
}

#handles a{
	color: #E6E6E6;
}

#rims{
	background: url(../images/mletzko_08_details_02_011.png) rgba(0, 0, 0, 0.55) center / cover no-repeat;
	text-align: center;
	color: #E6E6E6;
}

#rims .row{ 
	height: 940px;
}

#rims h3{
	font-family: 'Mate Italic';
	text-transform: lowercase;
	font-size: 43px;
	line-height: 52px;
	padding-right: 7%;
}

#rims h4{
	display: block;
	font-family: 'HelveticaNeue Bold';
	text-transform: uppercase;
	font-size: 63px;
	line-height: 74px;
	margin-bottom: 1em;
	letter-spacing: 2.77px;
}

#rims h2{
	font-family: 'HelveticaNeue Light';
	font-size: 20px;
	line-height: 21px;
}

#rims a{
	color: #E6E6E6;
}

#bodyhr{
	background: url(../images/mletzko_03_details_047.png) rgba(0, 0, 0, 0.55) center / cover no-repeat;
	text-align: center;
	color: #E6E6E6;
}

#bodyhr .row{ 
	height: 940px;
}

#bodyhr h3{
	font-family: 'Mate Italic';
	text-transform: lowercase;
	font-size: 43px;
	line-height: 52px;
}

#bodyhr h3 span{
	display: block;
	font-family: 'HelveticaNeue Bold';
	text-transform: uppercase;
	font-size: 63px;
	line-height: 74px;
	margin-bottom: 1em;
	letter-spacing: 2.77px;
}

#bodyhr h2{
	font-family: 'HelveticaNeue Light';
	font-size: 20px;
	line-height: 21px;
}

#bodyhr a{
	color: #E6E6E6;
}

#engine{
	background: url(../images/mletzko_06_motor_details_003.png) rgba(0, 0, 0, 0.55) center / cover no-repeat;
	text-align: center;
	color: #E6E6E6;
}

#engine .row{ 
	height: 940px;
}

#engine h3{
	font-family: 'Mate Italic';
	text-transform: lowercase;
	font-size: 43px;
	line-height: 52px;
}

#engine h3 span{
	display: block;
	font-family: 'HelveticaNeue Bold';
	text-transform: uppercase;
	font-size: 63px;
	line-height: 74px;
	margin-bottom: 1em;
	letter-spacing: 2.77px;
}

#engine h2{
	font-family: 'HelveticaNeue Light';
	font-size: 20px;
	line-height: 21px;
}

#engine a{
	color: #E6E6E6;
}

#cockpit{
	background: url(../images/mletzko_10_interieur_details_004.png) rgba(0, 0, 0, 0.55) center / cover no-repeat;
	text-align: center;
	color: #E6E6E6;
}

#cockpit .row{ 
	height: 940px;
}

#cockpit h3{
	font-family: 'Mate Italic';
	text-transform: lowercase;
	font-size: 43px;
	line-height: 52px;
	padding-right: 7%;
}

#cockpit h4{
	display: block;
	font-family: 'HelveticaNeue Bold';
	text-transform: uppercase;
	font-size: 63px;
	line-height: 74px;
	margin-bottom: 1em;
	letter-spacing: 2.77px;
}

#cockpit h2{
	font-family: 'HelveticaNeue Light';
	font-size: 20px;
	line-height: 21px;
}

#cockpit a{
	color: #E6E6E6;
}

#doorPin{
	background: url(../images/mletzko_10_interieur_details_006.png) rgba(0, 0, 0, 0.55) center / cover no-repeat;
	text-align: center;
	color: #E6E6E6;
}

#doorPin .row{ 
	height: 940px;
}

#doorPin h3{
	font-family: 'Mate Italic';
	text-transform: lowercase;
	font-size: 43px;
	line-height: 52px;
}

#doorPin h3 span{
	display: block;
	font-family: 'HelveticaNeue Bold';
	text-transform: uppercase;
	font-size: 63px;
	line-height: 74px;
	margin-bottom: 1em;
	letter-spacing: 2.77px;
}

#doorPin h2{
	font-family: 'HelveticaNeue Light';
	font-size: 20px;
	line-height: 21px;
}

#doorPin a{
	color: #E6E6E6;
}

@media screen and (max-width: 992px) {
	#pageCard ul{margin: 0 0 0em;} 
	.container {width:90%;} 
	#pageCard ul li {margin-bottom: 1.5em;} 
	#fillerFlap h3 {font-size: 30px;line-height: 37px;}
	#fillerFlap h4{font-size: 46px;line-height: 51px;letter-spacing: 1.52px;}
	#outsideMirror h3 {font-size: 30px;line-height: 37px;}
	#outsideMirror h3 span{font-size: 46px;line-height: 51px;letter-spacing: 1.52px;}
	#headlights h3 {font-size: 30px;line-height: 37px;}
	#headlights h4{font-size: 46px;line-height: 51px;letter-spacing: 1.52px;}
	#handles h3 {font-size: 30px;line-height: 37px;}
	#handles h3 span{font-size: 46px;line-height: 51px;letter-spacing: 1.52px;}
	#rims h3 {font-size: 30px;line-height: 37px;}
	#rims h4{font-size: 46px;line-height: 51px;letter-spacing: 1.52px;}
	#bodyhr h3 {font-size: 30px;line-height: 37px;}
	#bodyhr h3 span{font-size: 46px;line-height: 51px;letter-spacing: 1.52px;}
	#engine h3 {font-size: 30px;line-height: 37px;}
	#engine h3 span{font-size: 46px;line-height: 51px;letter-spacing: 1.52px;}
	#cockpit h3 {font-size: 30px;line-height: 37px;}
	#cockpit h4{font-size: 46px;line-height: 51px;letter-spacing: 1.52px;}
	#doorPin h3 {font-size: 30px;line-height: 37px;}
	#doorPin h3 span{font-size: 46px;line-height: 51px;letter-spacing: 1.52px;}
	#menu-primary-menu li{width:100%;margin:0px}
	#pageCard ul li{margin-bottom: 27px;height:167px;}
}

@media only screen and (min-device-width:1024px) and (max-device-width:1366px) and (-webkit-min-device-pixel-ratio:2) and (orientation:landscape)
{
	#menu-primary-menu li{margin-left:0px}
	.overlay-content ul{margin-left: 12em!important;}
	footer .wpcf7-response-output {
		bottom: 15%!important;
	}
}

@media only screen and (min-device-width:1024px) and (max-device-width:1366px) and (-webkit-min-device-pixel-ratio:2) and (orientation:portrait)
{
	#menu-primary-menu li{margin-left:0px}
	.overlay-content ul{margin-left:0!important;margin-top: 2%;}
	footer .wpcf7-response-output {
		bottom: 15%!important;
	}
}

/* Love Details End - Soumik */



.cursive_text{
	font: Italic 20px/24px Mate;
	letter-spacing: 0;
	color: #E6E6E6;
	opacity: 1;
}
.bottom-full-image .cursive_text{
	font: Italic 32px/39px Mate;
}

.bold_text{
	font: Bold 29px/34px Work Sans;
}

.bottom-full-image .bold_text{
	font: Bold 29px/34px Work Sans;
}

.helvetica_text{
	font: Helvetica 75 Bold 24px/28px Helvetica Neue;
	letter-spacing: 1.44px;
}

/*p .bold_text{
	font: Helvetica 75 Bold 24px/28px Helvetica Neue;
	letter-spacing: 1.44px;
	color: #E6E6E6;
}*/

/** Above css is for Index page - added by Aadarsh Jain **/

/** Below css is for Footer page - added by Aadarsh Jain **/
footer{	
	background: #222222;
	color: #E6E6E6;
	padding: 6em 0 0;
}

footer .widget{
	padding: 0;
    border: 0;
    margin: 0;
}
footer input[type="submit"]:hover{
	background: transparent;
}

footer h5,
footer .widget .widget-title,
footer .widget h5{
	font-family: 'Work Sans Bold';
	font-size: 29px;
	line-height: 34px;
	letter-spacing: 1.16px;
	text-transform: uppercase;
	margin-bottom: 2em;
}

footer p{
	font-family: 'Work Sans Light';
	font-size: 20px;
	line-height: 32px;
	letter-spacing: 1.2px;
	margin-bottom: 1.3em;
	color: #E6E6E6;
}
footer ul{
	margin: 0;
	padding: 0;
	list-style: none;
}

footer form ul li{
	float: left;
	/* margin-bottom: 2em; */
}
footer form ul li:first-child{
	width: 90%;
}
footer form ul li:nth-child(2){
	width: 100%;
	float: right;
}
footer form ul li:last-child{
	margin-bottom: 6em;
}

footer form ul li input[type="email"]{
	background: none;
	color: #4B4B4B;
	font-family: 'Work Sans Medium';
	font-size: 20px;
	line-height: 32px;
	letter-spacing: 1.2px;
	border-left: 0;
	border-right: 0;
	border-top: 0;
	border-width: 2px;
	border-color: #6F6F6F;
	padding-right: 50px;
}

footer ul.socialMenu li{
	float: left;
	margin-right: 10px;
}
footer ul.socialMenu li img{
	width: 32px;
	height: 32px;
}

footer #footerSideMenu ul li{
	font-family: 'Work Sans Medium';
	font-size: 20px;
	line-height: 55px;
	letter-spacing: 0.8px;
}

footer #footerTop{
	padding-bottom: 6em;
}

footer #footerBottom,
footer #footerBottom p {
	background: #101010;
	font-family: 'Work Sans';
	font-size: 14px;
	line-height: 32px;
	letter-spacing: 0.84px;
}

footer ul#menu-social-menu li a{
	text-indent: -9999px;
	height:32px;
	width:32px;
	display: block;
	background-size: 100% auto;
	background-repeat: no-repeat;
	background-position: center;
}

footer ul#menu-social-menu li.facebook a{
	background-image:url(../images/iconFacebook.svg);
}

footer ul#menu-social-menu li.instagram a{
	background-image:url(../images/iconInstagram.svg);
}

footer ul#menu-social-menu li.twitter a{
	background-image:url(../images/iconTwitter.svg);
}
footer .wpcf7-response-output {
	border: 2px solid #ffffff;
    position: absolute;
    left: 0;
    padding: 2%;
    bottom: 8%;
}
footer  input[type="submit"]:focus{
    background: transparent;
}
footer #footerBottom ul{
	position: absolute;
    z-index: 9999;
}

footer #footerBottom ul li{
	float: left;
	margin-right: 30px;
}
footer #footerBottom ul li a{
	color: #E6E6E6;
}

footer a{
	color: #E6E6E6;	
}

a{
	text-decoration: none;
}
.footer-contact-website{
	color: #E6E6E6;		
}
footer ul#menu-social-menu li a{
    text-indent: -9999px;
    height:32px;
    width:32px;
    display: block;
    background-size: 100% auto;
    background-repeat: no-repeat;
    background-position: center;
}

footer ul#menu-social-menu li.facebook a{
    background-image:url(../images/iconFacebook.svg);
}

footer ul#menu-social-menu li.instagram a{
    background-image:url(../images/iconInstagram.svg);
}

footer ul#menu-social-menu li.twitter a{
    background-image:url(../images/iconTwitter.svg);
}

.footer-main-div{
    padding-left: 80px;
    padding-top: 110px;
}

.column {
  float: left;
  width: 33.33%;
}
/*akshay start*/
footer input.wpcf7-form-control.wpcf7-submit {
	background: #222222;
    position: absolute;
    top: 28%;
    right: 11%;
}

footer input[type="checkbox" i]:before {
    content: "";
    height: 23px;
    width: 23px;
    background: #222222;
    display: block;
    /* z-index: 9999; */
    border: 1px solid #E6E6E6;
	opacity: 0.9
}
footer span.wpcf7-list-item{
	margin:0;
}
footer input[type=checkbox]{
    height: 23px;
	width: 23px;
	position: relative;
	top: 6px;	;
}
footer .clearfix span,.clearfix a{
	text-decoration: underline;
	position: relative;
	bottom: 0;
	float: left;
}
footer .clearfix .wpcf7-not-valid-tip {
    position: absolute;
    width: 100%;
    min-width: 200px;
    bottom: -25px;
}
footer span.wpcf7-list-item-label {
    display: none;
}
footer input[type="submit"]:focus{
	outline: none;
}
footer .mt-2,footer .my-2 {
    margin-top: 0.7rem!important;
}
@media screen and (max-width: 360px){
	footer input.wpcf7-form-control.wpcf7-submit {
		bottom: 45%
	}
}
/*akshay end*/
/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
  text-align: center;
}

.footer-bottom{
	background: #101010 0% 0% no-repeat padding-box;
	position: absolute;
    width: 100%;
    text-align: center;
    bottom:0;
    font: Regular 08px/19px sans-serif;
	letter-spacing: 0.84px;
	padding-top: 10px;
	height: 40px;
}

.footer-contact-head{
	/*top: 3968px;*/
	left: 144px;
	width: 144px;
	
	text-align: left;
	font: Bold 18px/23px sans-serif;
	letter-spacing: 1.16px;
	text-transform: uppercase;
}

.footer-contact-details{
	/*top: 4040px;*/
	left: 144px;
	width: 499px;
	
	text-align: left;
	letter-spacing: 1.2px;

	font: Regular 14px/19px sans-serif;
	color: #c3c3c3;	
}

.footer-touch-email-head
{
	/*top: 3968px;*/
	left: 659px;
	width: 329px;
	
	text-align: left;
	font: Bold 18px/23px sans-serif;
	letter-spacing: 1.16px;
	text-transform: uppercase;
}

.footer-socialmedia-div{
	/*top: 4247px;*/
	left: 659px;
	width: 142px;
	padding-top: 60px;
	
}
.socialmedia-icon{
	/*top: 4247px;*/
	left: 659px;
	width: 32px;
	height: 32px;
}

.footer-next-step-head{
	/*top: 3968px;*/
	left: 1380px;
	width: 170px;
	
	text-align: left;
	font: Bold 18px/23px sans-serif;
	letter-spacing: 1.16px;
	text-transform: uppercase;
}

.footer-next-step-subhead{
	/*top: 4231px;*/
	left: 1380px;
	width: 499px;
	
	text-align: left;
	font: Medium 20px/55px sans-serif;
	letter-spacing: 0.8px;
}

.footer-bottom-left{
	font: Regular 08px/15px sans-serif;
}

.footer-bottom-center{
	font: Regular 08px/15px sans-serif;
}

.footer-contact-website{
	color: #E6E6E6;
}

/*footer svg{
	fill: #E6E6E6;
	text-decoration="underline"
}*/

/*footer #svg_holder{
	border-left: 0;
	border-right: 0;
	border-top: 0;
	border-bottom: 2px solid #6f6f6f;

	position: relative;
	padding: 23px;
	line-height: 32px;
}*/

/* footer input[type="submit"] {
    margin-left: -35px;
    margin-top: 12px;
    -webkit-appearance: none;
	position: absolute;
	fill : white;
} */

 #footerSideMenu a{
	transition: 0.4s;
}
  
#footerSideMenu a:hover, #footerSideMenu a:focus {
	color: #ffffff;
}

@media (max-width: 1024px) {
	footer #footerBottom ul{
		position: relative;
		margin:0;
		float: none;
	}

	footer #footerBottom p{
		text-align: left !important;
		padding-bottom: 10px;
	}

	footer #footerBottom,
	footer #footerBottom p {
		font-size: 11px;
		line-height: normal;
		letter-spacing: 0.84px;
		padding-top: 10px;
	}
		
	
	
	/*margin-right: 30px;*/
}

@media screen and (max-width: 992px) {
	#heartbeat{
		background: rgba(0, 0, 0, 0.6);
	}
	#heartbeat h3 {
		text-align: center;
		font-size: 30px;
		line-height: 37px;
	}
	#heartbeat h4 {
		text-align: center;
		font-size: 46px;
		line-height: 51px;
		letter-spacing: 1.52px;
		margin-left: 0;
		margin-top: 3%;
	}
	#heartbeat h2 {
		font-size: 14px;
		line-height: 32px;
		margin-top: 5%;
		letter-spacing: 0.84px;
		margin-left: 6%;
		text-align: center;
	}
}
@media screen and (max-width: 320px) {
	

	#heartbeat{
		background: rgba(0, 0, 0, 0.6);
	}
	#heartbeat h3 {
		font-size: 30px;
		line-height: 37px;
		margin-left: 9%;
	}
	#heartbeat h4 {
		font-size: 46px;
		line-height: 51px;
		letter-spacing: 1.52px;
		margin-left: 11%;
		margin-top: 3%;
	}
	#heartbeat h2 {
		font-size: 14px;
		line-height: 32px;
		margin-top: 15%;
		letter-spacing: 0.84px;
		margin-left: 6%;
		text-align: center;
	}
	.overlay{
		overflow: auto;
	}
	.smallDev{
		position: absolute;
		top: 50%;
	}
}

@media screen and (max-width: 1440px) and (min-width: 992px) {
	.overlay ul {
		padding-top: 0%;
	}
	
	#menu-primary-menu li{ margin-left: 1em;}
}
@media screen and (max-width: 1366px) and (min-width: 992px){
	.overlay ul {
		margin-left: 12em;
	}
}
@media screen and (max-width: 1680px) and (min-width: 1400px) {
	.overlay ul {
		padding-top: 0%;
		margin-left: 12em;
	}
}

/*Akshay css start for car no one*/

#bannerCanNoOne h3,.details h3 {
    font-family: 'Mate Italic';
	color: #E6E6E6;
	text-transform: lowercase;
    font-size: 32px;
    line-height: 39px;
}

#bannerCanNoOne h4,.details h4 {
    text-transform: uppercase;
    font-family: 'Work Sans Bold';
    font-size: 35px;
    line-height: 52px;
    letter-spacing: 2.64px;
    padding-bottom: 25px;
}

.details p {
    font-size: 20px;
    line-height: 55px;
    font-family: 'Work Sans Light';
	padding: 0 0 5em;
	text-align: center;
	color: #101010;
	text-transform: lowercase;
}


.slider {
    margin-top: 8.7em;
}

.slider  .bx-wrapper .bx-pager, .bx-wrapper .bx-controls-auto{
	bottom: 50px;
}

.slider .bx-wrapper{
	margin-bottom: 0;
}

#sliderExterior .bx-wrapper {
    padding: 0 0;
}
.bannerCanNoOneBackground{
	height: 100vh;
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
}
#bannerCanNoOne {
	background: rgba(0, 0, 0, 0.55) center / cover no-repeat;
    text-align: center;
    color: #E6E6E6;
	position: absolute;
	top: 0;
	width: 100%;
    height: 100vh;
}
#bannerCanNoOne .row {
    height: 100vh;
}
#bannerCanNoOne h4{
	color: #E6E6E6;
	font-size: 63px;
	margin-left: 4%;
	margin-top: 2%;
}
#bannerCanNoOne h3{
	color: #E6E6E6;
	font-size: 47px;
}
#bannerCanNoOne p{
	color: #E6E6E6;
	font-size: 20px;
	font-family: 'HelveticaNeue Light';
	margin-left: 20%;
}
.slider .carousel-control-prev-icon{
	left: 0;
     background: url(../images/controls.png) no-repeat 0 -32px;
	background: url(../images/arrowLeft.svg) no-repeat;
	height: 100px;
    width: 100px;
} 

.slider .carousel-control-next-icon{
	right: 0;
	background: url(../images/arrowRight.svg) no-repeat;
	height: 100px;
    width: 100px;
} 
section#exteriorDetails {
    margin: 40px;
}

.carousel-indicators li{
	border: 1px solid #484848;
    text-indent: -9999px;
    display: block;
    width: 15px;
    height: 15px;
    margin: 0 5px;
    outline: 0;
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
	border-radius: 10px;
	background: #fff!important;
}
.carousel-indicators li.active{
	
	background: #E6E6E6;
}
.carousel-indicators{
	bottom:3%;
}
.details{
    background: url(../images/logoBigTransparency.svg) no-repeat 100%;
    background-size: 100% auto;
    background-position: center;
    padding-top: 6.5em;
}
.details a{
	color: #000;
	text-transform: uppercase;
}
.details a:hover{
	color: #000;
}
#footerTop a:hover{
	color: #fff;
}
.details h2 {
    font-family: 'HelveticaNeue Bold';
    text-transform: uppercase;
    font-size: 24px;
    line-height: 28px;
    letter-spacing: 1.44px;
}
@media screen and (max-width: 1024px) {
	#bannerCanNoOne h4 {
		font-size: 42px;
	}
}
@media screen and (max-width: 992px) {
	.slider {
		margin-top: 3em;
	}
	.details{
		padding-top: 4em;
	}
	.details p {
		font-size: 15px;
		line-height: 42px;
		padding: 0 0 2em 0;
	}
	.details h2 {
		font-size: 14px;
	}
	.carNumOne .container {
		max-width: 100%;
	}
	footer form ul li:last-child{margin-bottom:54px;}
	#footerSideMenu {padding-top:79px;}
	footer #footerTop{padding-bottom: 4em;}
	footer input.wpcf7-form-control.wpcf7-submit {top: 16%;right: 10%;}
	footer p{font-size: 14px;line-height: 32px;letter-spacing: 0.21px;color: #E6E6E6;}
	footer ul#menu-social-menu li a{height:21.92px;width:21.92px;}
	footer #footerSideMenu ul li{font-size: 14px;line-height: 32px;letter-spacing: 0.21px;}
	footer h5,
	footer .widget .widget-title,
	footer .widget h5{font-size: 14px;line-height: 16px;letter-spacing: 0.84px;margin-top:1%}
	#getInTouch h2{font-size: 14px;}
	#getInTouch h3 span{font-size: 30px;} 
	#getInTouch h3{font-size: 20px;}
	#bannerCanNoOne .text_align {
		position: absolute;
		top: 34%;
	}
	#bannerCanNoOne h3 {
		font-size: 30px;
		text-align: center;
	}
	#bannerCanNoOne p {
		font-size: 20px;
		margin-left: 0%;
		text-align: center;
	}
	#bannerCanNoOne h4 {
		font-size: 42px;
		margin-left: 4%;
		margin-top: 2%;
		text-align: center;
	}
  }

/*Akshay css end for car no one*/
/** Above css is for Footer page - added by Aadarsh Jain **/
/** Above css is for Footer page - added by Aadarsh Jain **/

/* Below is the css for My Mletzko page */
#mymeltzko_div {
	width: 100%;
	background: rgba(0, 0, 0, 0.6) top center / cover no-repeat;
	height: 100vh;
}
#mymeltzko_container, #mymeltzko_row{	
	height: 100vh;
}

#mymeltzko_div h2 {
	font-size: 32px;
	font-family: 'Mate Italic';
	line-height: 39px;
	text-transform: lowercase;
	color: #E6E6E6;
	letter-spacing: 0;
	margin-bottom: 18px;
}

#mymeltzko_div h2 span {
	font-family: 'HelveticaNeue Bold';
	text-transform: uppercase;
	display: block;
	font-size: 42px;
	line-height: 49px;
	color: #E6E6E6;
	letter-spacing: 2.52px;
	margin-top: 10px;
}

#mymeltzko_div form ul li:last-child {
	float: right;
	font-size: 24px;
	letter-spacing: 1.44px;
	line-height: 28px;
	font-family: 'HelveticaNeue Bold';
	text-transform: uppercase;
	color: #E6E6E6;
	margin: 0;
}

#mymeltzko_div form ul li:last-child input {
	padding-right: 0;
}

/* #mymeltzko_div form ul li:first-child{
	margin-bottom: 15px;
} */

#mymeltzko_div input {
	background: none;
	border-left: 0;
	border-right: 0;
	border-top: 0;
	border-width: 1px;
	border-color: #E6E6E6;
	color: #E6E6E6;
	font-size: 20px;
	line-height: 32px;
	font-family: 'HelveticaNeue Medium';
	letter-spacing: 1.2px;
	padding-left: 0;
}

#mymeltzko_div input:-ms-input-placeholder {
	color: #4B4B4B;
	margin-bottom: 15px;
}

#mymeltzko_div div.wpcf7-response-output{
	width: 90%;
    text-align: left;
    color: #fff;
    letter-spacing: 1px;
    margin: 6em 1.6em 1em;
    padding: 1em;
    line-height: normal;
}

/* Added code for the */
#mymeltzko_div input:invalid {
	box-shadow: none;
}

#mymeltzko_div form ul li {
	color: #4B4B4B;
	margin: 0 25px 18px;
}

@media only screen and (min-device-width: 767px) and (max-device-width: 1023px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: landscape) {
	
	#mymeltzko_div h2 {				
		margin-bottom: 0;
	}
	#mymeltzko_div h2 span {		
		line-height: 28px;		
		margin-top: 0;
	}

	#mymeltzko_div form ul li {
		margin-bottom: 0;
	}
	.customWidth{
		margin-top: 2rem !important;
	}

	.playButtonImage{
		max-width: 100px;
	}
	#mymeltzko_div{
		height: 100%;
		padding: 5em 0;
		background-position-x: center!important;
	}
}

@media (max-width: 992px) {
	#mymeltzko_div {
		background-position-x: 90%;
	}
	#mymeltzko_div div.wpcf7-response-output{
		width: 85%;
	}	
}

@media only screen and (min-device-width: 1024px) and (max-device-width: 1366px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: portrait) {
	.customWidth {
		max-width: 90%;
		flex: none;
	}
}

@media only screen and (min-device-width: 1024px) and (max-device-width: 1366px) and (-webkit-min-device-pixel-ratio: 2)  and (orientation: landscape)  {
	.customWidth {
		max-width: 90%;
		flex: none;
	}
}

@media (max-width: 767px) and (orientation: landscape)  {

	#mymeltzko_row, #mymeltzko_container,#mymeltzko_div{
		min-height: 500px;
		height: auto;
	}
	
	.playButtonImage{
		max-width: 100px;
	}
	#mymeltzko_div{
		background-position-x: center;
	}
}

@media (max-width: 767px) and (orientation: portrait)  {

	.playButtonImage{
		max-width: 60px;
	}
	
	#mymeltzko_div{
		height: 100%;
		padding: 4em 0;
	}

}
/* home page video button classes*/
#div_video{
	position: relative;
	cursor: pointer;
}
#playButton{
	z-index: 10;
	position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%,-50%);
}
#videoPlay{
	object-fit: cover;
}



/* home page video button classes*/

/** Above is the css for My Mletzko page**/
.entry-header .entry-title{
	text-transform: uppercase;
	font-family: 'Work Sans Bold';
	font-size: 44px;
	line-height: 52px;
	letter-spacing: 2.64px;
}

.entry-page-content{
	font-family: 'HelveticaNeue Light';
	font-size: 20px;
	line-height: normal;
	letter-spacing: 1.2px;
	line-height: 30px;
	color: #222222;
}
.entry-page-content p{
	margin-bottom: 1rem;
}
#defaultContent{
	padding: 12em 0 7em;
}
span.wpcf7-not-valid-tip {
    margin-top: 2%;
}