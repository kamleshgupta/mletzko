<?php
  /* Template Name: Car No 1 */ 

    get_header();
    
    //$lang = strtolower($_COOKIE['lang']); //"en";
    $lang = isset($_COOKIE['lang']) ? strtolower($_COOKIE['lang']) : 'en';

    // Section First
    $bannerBackground = "wpcf-banner-background";
    $bannerCFirst = "wpcf-banner-content-first";
    $bannerCSecond = "wpcf-banner-content-second";
    $bannerCThird = "wpcf-banner-content-third";

    // Exterior Content
    $extPageContent = "wpcf-exterior-page-content";
    $extImages = "wpcf-exterior-slider-images";

    // Interior Content
    $intPageContent = "wpcf-interior-page-content";
    $intImages = "wpcf-interior-slider-images";
    

    // Get In Touch Fields
    $background = "wpcf-set-background-image";
    $link = "wpcf-link-to-page";
    $gitHeading = "wpcf-git-heading";
    $gitSubHeading = "wpcf-git-sub-heading";
    $addheading = "wpcf-additional-heading";    

    global $post; 
?>
<?php while ( have_posts() ): the_post(); ?>
<!-- Start Your Coding After this Line -->

<div class="overlapPanel" ></div>
<section id="bannerCanNoOne" style="background-image: linear-gradient(rgba(34, 34, 34, 0.6),rgba(34, 34, 34, 0.6)),url(<?php if ( get_post_meta($post->ID, $bannerBackground, true)) { echo do_shortcode('[types field="'.ltrim($bannerBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);" class="overlapPanel bannerArea panel">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-start">    
            <div data-0="opacity: 1;transform: translate(0, 0px);" data-400="opacity: 1;transform: translate(0, -100px);" class="col-lg-10 col-sm-12 smallDev">
                <?php 
                    if ( get_post_meta($post->ID, $bannerCFirst."-".$lang, true)) {
                        echo  "<h3>" . do_shortcode('[types field="'.ltrim($bannerCFirst,"wpcf-")."-".$lang.'"]');

                        if ( get_post_meta($post->ID, $bannerCSecond."-".$lang, true)) { 
                            echo "<span>" . do_shortcode('[types field="'.ltrim($bannerCSecond,"wpcf-")."-".$lang.'"]') . "</span>";
                        }

                        echo "</h3>";
                    }

                    if ( get_post_meta($post->ID, $bannerCThird."-".$lang, true)) {
                        echo "<h4>" . do_shortcode('[types field="'.ltrim($bannerCThird,"wpcf-")."-".$lang.'"]') . "</h4>";
                    }
                ?>
                
            </div>
        </div>     
    </div>
</section>

<?php if ( get_post_meta($post->ID, $extPageContent."-".$lang, true)) { ?>
<section id="loveContent" class="details overlapPanel panel">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">
            <div class="contentDetail col">
                <?php
                    echo do_shortcode('[types field="'.ltrim($extPageContent,"wpcf-")."-".$lang.'" output="raw"]') ;
                ?>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<?php if ( get_post_meta($post->ID, $extImages, true)) { ?>
<section id="sliderExterior" class="slider">
    <div class="bxslider text-center w-100">
        <?php
            do_shortcode('[wpv-for-each field="'.$extImages.'"]') ;
                echo do_shortcode('[types field="'.ltrim($extImages,"wpcf-").'"]');
            do_shortcode('[/wpv-for-each]') ;
        ?>
    </div>
</section> 
<?php } ?>

<?php if ( get_post_meta($post->ID, $intPageContent."-".$lang, true)) { ?>
<section id="loveContent" class="details overlapPanel panel">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">
            <div class="contentDetail col">
                <?php
                    echo do_shortcode('[types field="'.ltrim($intPageContent,"wpcf-")."-".$lang.'" output="raw"]') ;
                ?>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<?php if ( get_post_meta($post->ID, $intImages, true)) { ?>
<section id="sliderInterior"  class="slider">
    <div class="bxslider text-center w-100">
        <?php
            do_shortcode('[wpv-for-each field="'.$intImages.'"]') ;
                echo do_shortcode('[types field="'.ltrim($intImages,"wpcf-").'"]');
            do_shortcode('[/wpv-for-each]') ;
        ?>
    </div>
</section> 
<?php } ?>

<?php
    $loop = new WP_Query( 'page_id=14' );

    if ( $loop->have_posts() ) : 

        while ($loop->have_posts()) : $loop->the_post();     
?>
<section id="getInTouch" class="panel" style="background-image: linear-gradient(rgba(34, 34, 34, 0.7),rgba(34, 34, 34, 0.7)),url(<?php if ( get_post_meta($post->ID, $background, true)) { echo do_shortcode('[types field="set-background-image" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">    
            <div class="col-lg-3 col-sm-12" data-4700="transform: translate(0, 0px);"  data-8000="transform: translate(0, -1000px);"> <!-- data-9600="filter:blur(2px)" data-10000="filter:blur(0px)" data-11000="filter:blur(10px)" -->
                <a href="<?php if ( get_post_meta($post->ID, $link, true)) { echo do_shortcode('[types field="link-to-page" output="raw"]'); } else { echo "#"; } ?>">
                    <h3>
                        <?php
                            if ( get_post_meta($post->ID, $gitHeading."-".$lang, true)) echo do_shortcode('[types field="'.ltrim($gitHeading,"wpcf-")."-".$lang.'"]');
                            if ( get_post_meta($post->ID, $gitSubHeading."-".$lang, true)) echo "<span>" . do_shortcode('[types field="'.ltrim($gitSubHeading,"wpcf-")."-".$lang.'"]') . "</span>" ;
                        ?>
                    </h3>

                    <h2>
                        <?php
                            if ( get_post_meta($post->ID, $addheading."-".$lang, true)) echo  do_shortcode('[types field="'.ltrim($addheading,"wpcf-")."-".$lang.'"]');
                        ?> &raquo;
                    </h2>
                </a>
            </div>
        </div>
    </div>
</section>
<?php 
        endwhile; wp_reset_query(); 
    endif;
?>

<?php endwhile; wp_reset_query(); ?>

<script>
    var width = (window.innerWidth > 0) ? window.innerWidth : document.documentElement.clientWidth;
    if (width > 1024) {

        $(function () { // wait for document ready
            // init
            var flag = false;
            var controller = new ScrollMagic.Controller({
                globalSceneOptions: {
                    triggerHook: 'onLeave',
                    duration: "0%"
                }
            });

            // get all slides
            var slides = document.querySelectorAll("section.panel");
            console.log(slides.length)
            // create scene for every slide
            for (var i = 0; i < slides.length; i++) {
                new ScrollMagic.Scene({
                        triggerElement: slides[i]
                    })
                    .setPin(slides[i], {
                        pushFollowers: false
                    })
                    .addIndicators() // add indicators (requires plugin)
                    .addTo(controller)
            }
            $(window).scroll(function (event) {
                var scroll = $(window).scrollTop();
                if (scroll > 4700) {
                    controller.enabled(false);
                    flag = true;
                } else if (scroll < 4700 && flag) {
                    controller = new ScrollMagic.Controller({
                        globalSceneOptions: {
                            triggerHook: 'onLeave',
                            duration: "0%"
                        }
                    });
                    var slides = document.querySelectorAll("section.panel");
                    for (var i = 0; i < slides.length; i++) {
                        new ScrollMagic.Scene({
                                triggerElement: slides[i]
                            })
                            .setPin(slides[i], {
                                pushFollowers: false
                            })
                            .addIndicators() // add indicators (requires plugin)
                            .addTo(controller)
                    }
                    flag = false;
                }

            });
        });

    }
   
</script>

<?php get_footer(); ?>