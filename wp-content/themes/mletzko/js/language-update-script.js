
jQuery(document).ready(function(){
	setTimeout(function(){
		var lang = window.localStorage.getItem('lang') == 'DE' ? 'DE' : 'EN';
		toggleLanguage(lang);   
	}, 500);
});

jQuery('.language_selecter').on('click', function(){
	setTimeout(() => {
		var lang = jQuery(this).html();      
		toggleLanguage(lang,true);
	}, 500);
});

function toggleLanguage(value, reload){

	addPathInLocal();
	updateCookieAllPath(value);

	if(value == 'DE'){
		jQuery('.language_selecter').text('EN');
	}else{
		jQuery('.language_selecter').text('DE');
	}

	window.localStorage.setItem('lang',value);

	if(reload){
		window.location.reload();
	}
}

function updateCookieAllPath(value){
	var all_path 	= [];
	all_path 		= localStorage.getItem("all_path") == null ? [] : JSON.parse(localStorage.getItem("all_path"));
	var home_url 	= '';

	document.cookie = "lang="+value+"; path=/";

	jQuery.each(all_path, function(i, path){
		document.cookie = "lang="+value+"; path="+path;
	});
}

function addPathInLocal(){
	var current_path 	= window.location.pathname;
	
	if(current_path.substring(current_path.length-1) == "/"){
		current_path 	= current_path.substring(0, current_path.length-1);
	}

	var all_path	= [];
	all_path 		= localStorage.getItem("all_path") == null ? [] : JSON.parse(localStorage.getItem("all_path"));

	if(jQuery.inArray(current_path, all_path) == -1){
		all_path.push(current_path);
		all_path.push(current_path+'/');
		localStorage.setItem("all_path", JSON.stringify(all_path));
	}
}