jQuery(document).ready(function(){
    var lang = window.localStorage.getItem('lang') == 'DE' ? 'DE' : 'EN';      
    toggleLanguage(lang);  
  });

  jQuery('.language_selecter').on('click', function(){
    var lang = jQuery(this).html();      
    toggleLanguage(lang,true);
  });

  function toggleLanguage(value, reload){

    document.cookie = "lang="+value+"; path="+window.location.hostname; 
      //document.cookie = "name=" + cookievalue + "; path=/";
      console.log(value);
    if(value == 'DE'){        
      jQuery('.language_selecter').text('EN');
    }else{        
      jQuery('.language_selecter').text('DE');
    }
    
    window.localStorage.setItem('lang',value);

    if(reload)          
      window.location.reload();
  }