<?php
/**
 * The template for the content bottom widget areas on posts and pages
 *
 * @package WordPress
 * @subpackage Mletzko
 * @since Mletzko 1.0
 */

if ( ! is_active_sidebar( 'sidebar-2' ) && ! is_active_sidebar( 'sidebar-3' ) ) {
	return;
}

$lang = strtolower($_COOKIE['lang']); //"en";

// If we get this far, we have widgets. Let's do this.
?>
<aside id="content-bottom-widgets" class="content-bottom-widgets" role="complementary">
	<?php if ($lang == "en") { ?>
		<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
			<div class="widget-area">
				<?php dynamic_sidebar( 'sidebar-2' ); ?>
			</div><!-- .widget-area -->
		<?php endif; ?>
	<?php } else { ?>
		<?php if ( is_active_sidebar( 'sidebar-2-de' ) ) : ?>
			<div class="widget-area">
				<?php dynamic_sidebar( 'sidebar-2-de' ); ?>
			</div><!-- .widget-area -->
		<?php endif; ?>
	<?php } ?>
	

	<?php if ( is_active_sidebar( 'sidebar-3' ) ) : ?>
		<div class="widget-area">
			<?php dynamic_sidebar( 'sidebar-3' ); ?>
		</div><!-- .widget-area -->
	<?php endif; ?>
</aside><!-- .content-bottom-widgets -->
