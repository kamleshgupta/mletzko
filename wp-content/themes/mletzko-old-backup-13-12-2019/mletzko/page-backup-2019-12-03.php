<?php
    /**
     * The template for displaying pages
     *
     * This is the template that displays all pages by default.
     * Please note that this is the WordPress construct of pages and that
     * other "pages" on your WordPress site will use a different template.
     *
     * @package WordPress
     * @subpackage Mletzko
     * @since Mletzko 1.0
     */

    get_header();

    $lang = strtolower($_COOKIE['lang']); //"en";
    $heading = "wpcf-heading";
    $subHeading = "wpcf-sub-heading";

    $pageContent = "wpcf-spage-content";

?>

<?php while ( have_posts() ): the_post(); ?>
    <section id="defaultContent">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-center">
                <div class="col-xl-8 col-lg-9">
                    <h2 class='entry-title'>
                        <?php 
                            if ( get_post_meta($post->ID, $heading."-".$lang, true)) {
                                echo  do_shortcode('[types field="'.ltrim($heading,"wpcf-")."-".$lang.'"]');

                                if ( get_post_meta($post->ID, $subHeading."-".$lang, true)) { 
                                    echo "<span>" . do_shortcode('[types field="'.ltrim($subHeading,"wpcf-")."-".$lang.'"]') . "</span>";
                                }
                            } else { 
                                the_title(); 
                            }
                        ?>
                    </h2>

                    <?php
                        if ( get_post_meta($post->ID, $pageContent."-".$lang, true)) {
                            echo "<div class='entry-page-content'>". do_shortcode('[types field="'.ltrim($pageContent,"wpcf-")."-".$lang.'"]') . "</div>";
                        }
                    ?>
                    
                </div>
            </div>
        </div>
    </section>
<?php endwhile; wp_reset_query(); ?> 

<?php get_footer(); ?>