<?php
  /* Template Name: Mletzko Story */ 
    get_header();
    $template_url = get_bloginfo('template_url');

    //$lang = strtolower($_COOKIE['lang']); //"en";
    $lang = isset($_COOKIE['lang']) ? strtolower($_COOKIE['lang']) : 'en';


    $secFirBackground = "wpcf-sec-fir-background";
    $secFirCFirst = "wpcf-sec1-string-first";
    $secFirCSecond = "wpcf-sec1-string-second";
    $secFirCThird = "wpcf-sec1-string-third";
    $background = "wpcf-set-background-image";
    $link = "wpcf-link-to-page";
    $gitHeading = "wpcf-git-heading";
    $gitSubHeading = "wpcf-git-sub-heading";
    $addheading = "wpcf-additional-heading";
    $extImages = "wpcf-exterior-slider-images";
    $pageContent = "wpcf-spage-content";
    global $post; 
    ?>
<div class="overlapPanel " ></div>
<section id="heartbeat" style="background-image: url(<?php if ( get_post_meta($post->ID, $secFirBackground, true)) { echo do_shortcode('[types field="'.ltrim($secFirBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);" class="overlapPanel bannerArea panel">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-start">    
            <div data-0="opacity: 1;filter:blur(0px);transform: translate(0, 0px);transition-duration: 3s linear;" data-400="opacity: 1;transform: translate(0, -550px);transition-duration: 3s linear;" class="col-lg-7 col-sm-12 smallDev">
                <?php 
                    if ( get_post_meta($post->ID, $secFirCFirst."-".$lang, true)) {
                        echo  "<h3>" . do_shortcode('[types field="'.ltrim($secFirCFirst,"wpcf-")."-".$lang.'"]');

                        if ( get_post_meta($post->ID, $secFirCSecond."-".$lang, true)) { 
                            echo "<span>" . do_shortcode('[types field="'.ltrim($secFirCSecond,"wpcf-")."-".$lang.'"]') . "</span>";
                        }

                        echo "</h3>";
                    }

                    if ( get_post_meta($post->ID, $secFirCThird."-".$lang, true)) {
                        echo "<h4>" . do_shortcode('[types field="'.ltrim($secFirCThird,"wpcf-")."-".$lang.'"]') . "</h4>";
                    }
                ?>
                
            </div>
        </div>     
    </div>
</section>



<?php if ( get_post_meta($post->ID, $pageContent."-".$lang, true)) { ?>
<section id="loveContent" class="details overlapPanel panel">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">
            <div class="contentDetail col">
                
                    <?php
                        echo do_shortcode('[types field="'.ltrim($pageContent,"wpcf-")."-".$lang.'" output="raw"]') ;
                    ?>
                
            </div>
        </div>
    </div>
</section>
<?php } ?>


<?php if ( get_post_meta($post->ID, $extImages, true)) { ?>
<section id="sliderExterior" class="slider panel">
    <div class="bxslider text-center w-100">
        <?php
            do_shortcode('[wpv-for-each field="'.$extImages.'"]') ;
                echo do_shortcode('[types field="'.ltrim($extImages,"wpcf-").'"]');
            do_shortcode('[/wpv-for-each]') ;
        ?>
    </div>
</section> 
<?php } ?>


<?php
    $loop = new WP_Query( 'page_id=14' );

    if ( $loop->have_posts() ) : 

        while ($loop->have_posts()) : $loop->the_post();     
?>
<section id="getInTouch" class="panel" style="background-image: linear-gradient(rgba(34, 34, 34, 0.7),rgba(34, 34, 34, 0.7)),url(<?php if ( get_post_meta($post->ID, $background, true)) { echo do_shortcode('[types field="set-background-image" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">    
            <div class="col-lg-3 col-sm-12" data-9600="transform: translate(0, 0px);transition-duration: 3s linear;" data-10000="filter:blur(0px);" data-11000="transform: translate(0, -550px);transition-duration: 3s linear;"> <!-- data-9600="filter:blur(2px)" data-10000="filter:blur(0px)" data-11000="filter:blur(5px)" -->
                <a href="<?php if ( get_post_meta($post->ID, $link, true)) { echo do_shortcode('[types field="link-to-page" output="raw"]'); } else { echo "#"; } ?>">
                    <h3>
                        <?php
                            if ( get_post_meta($post->ID, $gitHeading."-".$lang, true)) echo do_shortcode('[types field="'.ltrim($gitHeading,"wpcf-")."-".$lang.'"]');
                            if ( get_post_meta($post->ID, $gitSubHeading."-".$lang, true)) echo "<span>" . do_shortcode('[types field="'.ltrim($gitSubHeading,"wpcf-")."-".$lang.'"]') . "</span>" ;
                        ?>
                    </h3>

                    <h2>
                        <?php
                            if ( get_post_meta($post->ID, $addheading."-".$lang, true)) echo  do_shortcode('[types field="'.ltrim($addheading,"wpcf-")."-".$lang.'"]');
                        ?> &raquo;
                    </h2>
                </a>
            </div>
        </div>
    </div>
</section>
<script>
    var width = (window.innerWidth > 0) ? window.innerWidth : document.documentElement.clientWidth;
        if(width > 992){

            $(function() { // wait for document ready
    // init
    var flag = false;
    var controller = new ScrollMagic.Controller({
        globalSceneOptions: {
            triggerHook: 'onLeave',
            duration: "0%"
        }
    });

    // get all slides
    var slides = document.querySelectorAll("section.panel");
    console.log(slides.length)
    // create scene for every slide
    for (var i = 0; i < slides.length; i++) {
        new ScrollMagic.Scene({
                triggerElement: slides[i]
            })
            .setPin(slides[i], {
                pushFollowers: false
            })
            .addIndicators() // add indicators (requires plugin)
            .addTo(controller)
    }
    $(window).scroll(function(event) {
        var scroll = $(window).scrollTop();
        if (scroll > 3000) {
            controller.enabled(false);
            flag = true;
        } else if (scroll < 3000 && flag) {
            controller = new ScrollMagic.Controller({
                globalSceneOptions: {
                    triggerHook: 'onLeave',
                    duration: "0%"
                }
            });
            var slides = document.querySelectorAll("section.panel");
            for (var i = 0; i < slides.length; i++) {
                new ScrollMagic.Scene({
                        triggerElement: slides[i]
                    })
                    .setPin(slides[i], {
                        pushFollowers: false
                    })
                    .addIndicators() // add indicators (requires plugin)
                    .addTo(controller)
            }
            flag=false;
        }

    });
});

        }
   
</script>
<?php 
        endwhile; wp_reset_query(); 
    endif;
?>










<?php get_footer(); ?>