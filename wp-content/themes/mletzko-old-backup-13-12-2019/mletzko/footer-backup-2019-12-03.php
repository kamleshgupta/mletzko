<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Mletzko
 * @since Mletzko 1.0
 */

  $template_url = get_bloginfo('template_url');

  $lang = strtolower($_COOKIE['lang']); //"en";
?>
    </div><!-- .site-content -->

    <footer>
        <div id="footerTop" class="container-fluid">
            <div class="row justify-content-between">
                <div class="col-lg-3 widget-area">
                    <?php if ($lang == "en") { ?>
                        <?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
                            <?php dynamic_sidebar( 'sidebar-2' ); ?>
                        <?php endif; ?>
                    <?php } else { ?>
                        <?php if ( is_active_sidebar( 'sidebar-2-de' ) ) : ?>
                            <?php dynamic_sidebar( 'sidebar-2-de' ); ?>
                        <?php endif; ?>
                    <?php } ?>
                </div><!-- .widget-area -->

                <div class="col-lg-5 letStayInTouch">
                    <?php if ($lang == "en") { ?>
                        <?php if ( is_active_sidebar( 'sidebar-middle' ) ) : ?>
                            <?php dynamic_sidebar( 'sidebar-middle' ); ?>
                        <?php endif; ?>
                    <?php } else { ?>
                        <?php if ( is_active_sidebar( 'sidebar-middle-de' ) ) : ?>
                            <?php dynamic_sidebar( 'sidebar-middle-de' ); ?>
                        <?php endif; ?>
                    <?php } ?>

                    <?php
                        echo do_shortcode('[contact-form-7 id="56" title="Footer Form"]');
                    ?>

                    <div class="clearfix"></div>
                    
                    <?php 
                        if ( has_nav_menu( 'footerSideMenu' ) ) : 
                        wp_nav_menu(
                            array(
                            'theme_location'  => 'social',
                            'menu' 			  => 'social',
                            'container'       => 'ul',
                            'menu_class'      => 'socialMenu',
                            )
                        );
                        endif;
                    ?>
                    
                </div>
                
                <div class="col-lg-3" id="footerSideMenu">
                <?php if ($lang == "en") { ?>
                    <?php if ( is_active_sidebar( 'sidebar-3' ) ) : ?>
                        <?php dynamic_sidebar( 'sidebar-3' ); ?>
                    <?php endif; ?>
                <?php } else { ?>
                    <?php if ( is_active_sidebar( 'sidebar-3-de' ) ) : ?>
                        <?php dynamic_sidebar( 'sidebar-3-de' ); ?>
                    <?php endif; ?>
                <?php } ?>
                </div>
               
            </div>
        </div>
        
        <div id="footerBottom">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4">
                        <?php if ( has_nav_menu( 'footerSideMenu' ) ) : ?>
                            <?php
                                if ($lang == "en") {
									wp_nav_menu(
										array(
											'theme_location'  => 'footerBootomMenu',
											'menu' 			  => 'footerBootomMenu',
											'container'       => 'ul',
											'menu_class'      => 'footer-side-menu',
										)
									);						
								} else {
									wp_nav_menu(
										array(
											'theme_location'  => 'footerBootomMenuDe',
											'menu' 			  => 'footerBootomMenu',
											'container'       => 'ul',
											'menu_class'      => 'footer-side-menu',
										)
									);		
								}
                            ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-12">
                        <p class="mb-0 py-0"><?php echo "Copyright ". date('Y'). ' | '; bloginfo( 'name' ); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div><!-- .site -->
<?php wp_footer(); ?>

    <script type="text/javascript" src="<?php echo get_template_directory_uri() . '/js/jquery.min.js' ?>"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() . '/js/jquery.bxslider.js' ?>"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() . '/js/popper.min.js' ?>"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() . '/js/bootstrap.min.js' ?>"></script>
    <!-- <script type="text/javascript" src="<?php // echo get_template_directory_uri() . '/js/custom-script.js' ?>"></script> -->

    <script type="text/javascript">
        jQuery(document).ready(function () {
            $(".wpcf7-form").trigger("reset");
            /* Open */
            document.getElementById("openNav").addEventListener("click", function(){
                document.getElementById("myNav").style.height = "100%";
            });

            /* Close */
            document.getElementById("closeNav").addEventListener("click", function(){
                document.getElementById("myNav").style.height = "0%";
            });
            document.getElementById("myNav").addEventListener("click", function(){
                document.getElementById("myNav").style.height = "0%";
            });
            jQuery('.bxslider').bxSlider({
                mode: 'fade',
                captions: true,
                slideWidth: 600,
                adaptiveHeight: true
            });



            $("footer form ul li input[type='email']").click(function(){
                if($("footer form ul li input[type='email']").is(":focus")){
                $("footer input.wpcf7-form-control.wpcf7-submit").css("color","#222222");
                }
            });
            $("footer form ul li input[type='email']").focusout(function(){
            $("footer input.wpcf7-form-control.wpcf7-submit").css("color","#fff");
            })

            /*phone number validation*/
            $(':input[name="numbers"]').keydown(function(e){
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        
               return false;
    } 
 

        });
        });
                   

  </script>
</body>
</html>
