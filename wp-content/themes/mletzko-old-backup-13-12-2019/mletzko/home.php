<?php
    /* Template Name: Home Page */ 

    get_header(); 
    $template_url = get_bloginfo('template_url');

    //$lang = strtolower($_COOKIE['lang']); //"en";
    $lang = isset($_COOKIE['lang']) ? strtolower($_COOKIE['lang']) : 'en';
    $heading = "wpcf-heading";
    $subHeading = "wpcf-sub-heading";


    // Get In Touch Fields
    $background = "wpcf-set-background-image";
    $link = "wpcf-link-to-page";
    $gitHeading = "wpcf-git-heading";
    $gitSubHeading = "wpcf-git-sub-heading";
    $addheading = "wpcf-additional-heading";

    global $post; 
?>
<?php while ( have_posts() ): the_post(); ?>
<section class="panel">
<div id="div_video" >
    <div id="playButton" onclick="playVideoButton()">
    <img class="playButtonImage" src="<?php echo $template_url?>/images/PlayButton.svg">    
    </div>

    <video id="videoPlay" preload="none" class="embed-responsive" playsinline="playsinline" muted="muted" loop="loop" poster="<?php echo do_shortcode('[types field="add-poster-for-video" output="raw"]'); ?>" onclick="playVideo(this)">
        <source src="<?php echo do_shortcode('[types field="add-video-file" output="raw"]'); ?>" type="video/mp4">
    </video>

</div>
</section>
<section id="slider" class="details home panel">
    <div class="container">
        <div class="row text-center  align-items-center justify-content-center">
            <div class="col-lg-12">
                <h3>Mletzko story</h3>
            </div>
            <div class="col-lg-12 pl-0 pr-0">
                <div   class="bxslider text-center w-100">
                    <?php
                        $sliderTitle = "wpcf-slider-title";
                        $sliderContent = "wpcf-slider-content";
                        
                        $i = 0;
                        while(++$i){
                            if(get_post_meta($post->ID, $sliderTitle."-".strval($i)."-".$lang, true) || get_post_meta($post->ID, $sliderContent."-".strval($i)."-".$lang, true)){
                                echo "<div class='indexContainer'>";
                                echo "<h4>" . do_shortcode('[types field="'.ltrim($sliderTitle,"wpcf-")."-".strval($i)."-".$lang.'"]') . "</h4>" ;
                                echo "<p>" . do_shortcode('[types field="'.ltrim($sliderContent,"wpcf-")."-".strval($i)."-".$lang.'" output="raw"]') . "</p>" ;
                                echo "</div>";
                            }
                            else{
                                break;
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
    $loop = new WP_Query(
        array(
            'post_type' => 'page',
            'order' => 'ASC',
            'tag' => 'featured',
            'posts_per_page' => 2
        )
    );
?>
<?php if ( $loop->have_posts() ) : ?>

    <!-- <section id="pageCard">
        <div class="container">
            <div class="row">
                <ul>
                    <?php 
                        while ($loop->have_posts()) : $loop->the_post(); 
                        $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'custom-thumb', false, '' );          

                        if ( has_post_thumbnail() ) { 
                            echo '<li style="background-image:  linear-gradient(rgba(34, 34, 34, 0.7),rgba(34, 34, 34, 0.7)),url('. $src[0].');">';
                            // echo '<div class="overlayCar" style="width:100%;height:100%"></div>';
                        } else {
                            echo '<li style="background-image:  linear-gradient(rgba(34, 34, 34, 0.7),rgba(34, 34, 34, 0.7)),url('. $template_url.'/images/imageNotAvailable.png);">';
                            // echo '<li class="overlayCar">';
                        }
                    ?>
                        <a href="<?php the_permalink();?>">
                            <?php
                                echo "<div data-1000='transform: translate(-58%, 0px);' data-1800='filter:blur(0px)' data-2400='transform: translate(-58%,, -100px);' class='vHMiddle'><h2>";
                                    if ( get_post_meta($post->ID, $heading."-".$lang, true)) echo do_shortcode('[types field="'.ltrim($heading,"wpcf-")."-".$lang.'"]');
                                    if ( get_post_meta($post->ID, $subHeading."-".$lang, true)) echo "<span>" . do_shortcode('[types field="'.ltrim($subHeading,"wpcf-")."-".$lang.'"]') . "</span>" ;
                                echo "</h2></div>";
                            ?>
                            
                        </a>
                        
                    <?php
                        echo '</li>';    
                        endwhile; wp_reset_query(); 
                    ?>
                </ul>
            </div>
        </div>
    </section> -->
<?php endif; ?>

<section  class="details home panel" style="background-image: linear-gradient(rgba(34, 34, 34, 0.8),rgba(34, 34, 34, 0.8)),url('http://mletzko.praesentation.wiethe.de/wp-content/uploads/2019/11/mletzko_03_details_009.png')">
            <div class="container-fluid">
                <div class="row align-items-center justify-content-center">
                    <div class="contentDetail col">
                        <h3>Love Details</h3>
                        <h4>XXX</h4>
                        <p class="col-md-8">Vor einer kleinen Apotheke in Osnabrück fing vor 40 Jahren alles an. Dort sah unser Inhaber Dirk Lührmann seinen Lieblingsporsche – einen 946 – zum allerersten Mal und er blieb es bis heute! Der Besitzer der Apotheke und des geliebten 946 hieß Mletzko.</p>

                    <h2><a href="http://mletzko.praesentation.wiethe.de/my-mletzko">tell me more »</a></h2>            </div>
                </div>
            </div>
        </section>

        <section  class="details home panel"  style="background-image: linear-gradient(rgba(34, 34, 34, 0.7),rgba(34, 34, 34, 0.7)),url('<?php echo $template_url; ?>/images/mletzko_05_heck_040_1.jpg')">
            <div class="container-fluid">
                <div class="row align-items-center justify-content-center">
                    <div class="contentDetail col">
                        <h3>Car No One</h3>
                        <h4>XXX</h4>
                        <p class="col-md-8">Vor einer kleinen Apotheke in Osnabrück fing vor 40 Jahren alles an. Dort sah unser Inhaber Dirk Lührmann seinen Lieblingsporsche – einen 946 – zum allerersten Mal und er blieb es bis heute! Der Besitzer der Apotheke und des geliebten 946 hieß Mletzko.</p>

                    <h2><a href="http://mletzko.praesentation.wiethe.de/my-mletzko">tell me more »</a></h2>            </div>
                </div>
            </div>
        </section>
<?php
    $loop = new WP_Query( 'page_id=14' );

    if ( $loop->have_posts() ) : 

        while ($loop->have_posts()) : $loop->the_post();     
?>
<section id="getInTouch" style="background-image: linear-gradient(rgba(34, 34, 34, 0.7),rgba(34, 34, 34, 0.7)),url(<?php if ( get_post_meta($post->ID, $background, true)) { echo do_shortcode('[types field="set-background-image" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container">
        <div class="row align-items-center justify-content-center">    
            <div class="p-0" data-2000='transform: translate(0, 0px);' data-2600='opacity: 2;' data-3000='transform: translate(0, -100px);'>
                <a href="<?php if ( get_post_meta($post->ID, $link, true)) { echo do_shortcode('[types field="link-to-page" output="raw"]'); } else { echo "#"; } ?>">
                    <h3>
                        <?php
                            if ( get_post_meta($post->ID, $gitHeading."-".$lang, true)) echo do_shortcode('[types field="'.ltrim($gitHeading,"wpcf-")."-".$lang.'"]');
                            if ( get_post_meta($post->ID, $gitSubHeading."-".$lang, true)) echo "<span>" . do_shortcode('[types field="'.ltrim($gitSubHeading,"wpcf-")."-".$lang.'"]') . "</span>" ;
                        ?>
                    </h3>

                    <h2>
                        <?php
                            if ( get_post_meta($post->ID, $addheading."-".$lang, true)) echo  do_shortcode('[types field="'.ltrim($addheading,"wpcf-")."-".$lang.'"]');
                        ?> &raquo;
                    </h2>
                </a>
            </div>
        </div>
    </div>
</section>
<?php 
        endwhile; wp_reset_query(); 
    endif;
?>

<!-- <section id="getInTouch" style="background-image: url(<?php // if ( get_post_meta($post->ID, $background, true)) { echo do_shortcode('[types field="set-background-image" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png'; } ?>);">
    <div class="container">
        <div class="row align-items-center justify-content-center">    
            <div class="col-2 p-0">
                <a href="<?php // if ( get_post_meta($post->ID, $link, true)) { echo do_shortcode('[types field="link-to-page" output="raw"]'); } else { echo "#"; } ?>">
                    <h3>
                        <?php
                            // if ( get_post_meta($post->ID, $gitHeading."-".$lang, true)) echo do_shortcode('[types field="'.ltrim($gitHeading,"wpcf-")."-".$lang.'"]');
                            // if ( get_post_meta($post->ID, $gitSubHeading."-".$lang, true)) echo "<span>" . do_shortcode('[types field="'.ltrim($gitSubHeading,"wpcf-")."-".$lang.'"]') . "</span>" ;
                        ?>
                    </h3>

                    <h2>
                        <?php
                            // if ( get_post_meta($post->ID, $addheading."-".$lang, true)) echo  do_shortcode('[types field="'.ltrim($addheading,"wpcf-")."-".$lang.'"]');
                        ?>
                    </h2>
                </a>
            </div>
        </div>
    </div>
</section> -->

<script>

    function playVideo (scope){ 
        console.log("AAA");
        var flag = $("#playButton").hasClass('videoPlaying') ? true : false;
        if(!flag){       
        $("#playButton").css('display','none');
        $("#playButton").addClass('videoPlaying');
        scope.play();
        }else{
            $("#playButton").css('display','block');
            $("#playButton").removeClass('videoPlaying');
            scope.pause();
        }
    }
    function playVideoButton (){
        console.log("AAAB");
        var flag = $("#playButton").hasClass('videoPlaying') ? true : false;
        if(!flag){       
            $("#playButton").css('display','none');
            $("#playButton").addClass('videoPlaying');
            $("#videoPlay")[0].play();
        }else{
            $("#playButton").css('display','block');
            $("#playButton").removeClass('videoPlaying');
            $("#videoPlay")[0].pause();
        }
       
    }
</script>

<!-- <?php
    // $loop = new WP_Query( 'page_id=14' );

    // if ( $loop->have_posts() ) : 

    //     while ($loop->have_posts()) : $loop->the_post();     

    //     $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'custom-thumb', false, '' );           
?>
<section id="getInTouch" style="background-image: url(<?php // echo $src[0]; ?>);">
    <div class="container">
        <div class="row align-items-center justify-content-center">    
            <div data-2000='opacity: 0;' data-2200='opacity: 1.5;' data-3000='opacity: 0;' class="col-2 p-0">
                <a href="<?php the_permalink();?>">
                    <h3>
                        <?php
                            // if ( get_post_meta($post->ID, $subHeading."-".$lang, true)) echo do_shortcode('[types field="'.ltrim($subHeading,"wpcf-")."-".$lang.'"]');
                            // if ( get_post_meta($post->ID, $heading."-".$lang, true)) echo "<span>" . do_shortcode('[types field="'.ltrim($heading,"wpcf-")."-".$lang.'"]') . "</span>" ;
                        ?>
                    </h3>
                    <h2>Get in touch »</h2>
                </a>
            </div>
        </div>
    </div>
</section>
<?php 
    //     endwhile; wp_reset_query(); 
    // endif;
?> -->
<script>
    var width = (window.innerWidth > 0) ? window.innerWidth : document.documentElement.clientWidth;
        if(width > 992){

            $(function() { // wait for document ready
    // init
    var flag = false;
    var controller = new ScrollMagic.Controller({
        globalSceneOptions: {
            triggerHook: 'onLeave',
            duration: "0%"
        }
    });

    // get all slides
    var slides = document.querySelectorAll("section.panel");
    console.log(slides.length)
    // create scene for every slide
    for (var i = 0; i < slides.length; i++) {
        new ScrollMagic.Scene({
                triggerElement: slides[i]
            })
            .setPin(slides[i], {
                pushFollowers: false
            })
            .addIndicators() // add indicators (requires plugin)
            .addTo(controller)
    }
    $(window).scroll(function(event) {
        var scroll = $(window).scrollTop();
        if (scroll > 3000) {
            controller.enabled(false);
            flag = true;
        } else if (scroll < 3000 && flag) {
            controller = new ScrollMagic.Controller({
                globalSceneOptions: {
                    triggerHook: 'onLeave',
                    duration: "0%"
                }
            });
            var slides = document.querySelectorAll("section.panel");
            for (var i = 0; i < slides.length; i++) {
                new ScrollMagic.Scene({
                        triggerElement: slides[i]
                    })
                    .setPin(slides[i], {
                        pushFollowers: false
                    })
                    .addIndicators() // add indicators (requires plugin)
                    .addTo(controller)
            }
            flag=false;
        }

    });
});

        }
   
</script>
<?php endwhile; wp_reset_query(); ?>
<?php get_footer(); ?>