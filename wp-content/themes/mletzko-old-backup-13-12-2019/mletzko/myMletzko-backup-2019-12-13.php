<?php
  /* Template Name: My Mletzko */ 

    get_header(); 

    //$lang = strtolower($_COOKIE['lang']); //"en";
    $lang = isset($_COOKIE['lang']) ? strtolower($_COOKIE['lang']) : 'en';
    $heading = "wpcf-heading";
    $subHeading = "wpcf-sub-heading";

    global $post; 
?>

<?php 

    while ( have_posts() ): the_post(); 

    $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'custom-thumb', false, '' );
?>

<!-- Start Your Coding After this Line -->
<section id="mymeltzko_div" style="background-image: linear-gradient(rgba(34, 34, 34, 0.6),rgba(34, 34, 34, 0.6)),url(<?php echo $src[0]; ?>);">
    <div class="container-fluid" id="mymeltzko_container" >
        <div class="row align-items-center justify-content-center text-center" id="mymeltzko_row">
            <div class="col-xl-4 col-lg-7 col-md-7 col-12 px-0 customWidth">
                <h2>
                    <?php
                        if ( get_post_meta($post->ID, $heading."-".$lang, true)) echo do_shortcode('[types field="'.ltrim($heading,"wpcf-")."-".$lang.'"]');
                        if ( get_post_meta($post->ID, $subHeading."-".$lang, true)) echo "<span class='d-block'>" . do_shortcode('[types field="'.ltrim($subHeading,"wpcf-")."-".$lang.'"]') . "</span>" ;
                    ?>
                </h2>
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</section>

<!-- End Your Coding Before this Line -->

<?php endwhile; wp_reset_query(); ?>

<?php get_footer(); ?>