<?php
  /* Template Name: Car No 1 */ 

    get_header();
    
    $lang = strtolower($_COOKIE['lang']); //"en";
    
    // Section First
    $bannerBackground = "wpcf-banner-background";
    $bannerCFirst = "wpcf-banner-content-first";
    $bannerCSecond = "wpcf-banner-content-second";
    $bannerCThird = "wpcf-banner-content-third";

    // Exterior Content
    $extPageContent = "wpcf-exterior-page-content";
    $extImages = "wpcf-exterior-slider-images";

    // Interior Content
    $intPageContent = "wpcf-interior-page-content";
    $intImages = "wpcf-interior-slider-images";
    

    // Get In Touch Fields
    $background = "wpcf-set-background-image";
    $link = "wpcf-link-to-page";
    $gitHeading = "wpcf-git-heading";
    $gitSubHeading = "wpcf-git-sub-heading";
    $addheading = "wpcf-additional-heading";    

    global $post; 
?>
<?php while ( have_posts() ): the_post(); ?>
<!-- Start Your Coding After this Line -->

<div class="overlapPanel" style="background-image: url(<?php if ( get_post_meta($post->ID, $bannerBackground, true)) { echo do_shortcode('[types field="'.ltrim($bannerBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);"></div>
<section id="bannerCanNoOne" class="overlapPanel bannerArea">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-start">    
            <div data-0="opacity: 1;filter:blur(0px);transform: translate(0, 0px);" data-400="opacity: 1;filter:blur(10px);transform: translate(0, -100px);" class="col-lg-8 col-sm-12 smallDev">
                <?php 
                    if ( get_post_meta($post->ID, $bannerCFirst."-".$lang, true)) {
                        echo  "<h3>" . do_shortcode('[types field="'.ltrim($bannerCFirst,"wpcf-")."-".$lang.'"]');

                        if ( get_post_meta($post->ID, $bannerCSecond."-".$lang, true)) { 
                            echo "<span>" . do_shortcode('[types field="'.ltrim($bannerCSecond,"wpcf-")."-".$lang.'"]') . "</span>";
                        }

                        echo "</h3>";
                    }

                    if ( get_post_meta($post->ID, $bannerCThird."-".$lang, true)) {
                        echo "<h4>" . do_shortcode('[types field="'.ltrim($bannerCThird,"wpcf-")."-".$lang.'"]') . "</h4>";
                    }
                ?>
                
            </div>
        </div>     
    </div>
</section>

<?php if ( get_post_meta($post->ID, $extPageContent."-".$lang, true)) { ?>
<section id="loveContent" class="details overlapPanel">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">
            <div class="contentDetail col">
                <?php
                    echo do_shortcode('[types field="'.ltrim($extPageContent,"wpcf-")."-".$lang.'" output="raw"]') ;
                ?>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<?php if ( get_post_meta($post->ID, $extImages, true)) { ?>
<section id="sliderExterior" class="slider">
    <div class="bxslider text-center w-100">
        <?php
            do_shortcode('[wpv-for-each field="'.$extImages.'"]') ;
                echo do_shortcode('[types field="'.ltrim($extImages,"wpcf-").'"]');
            do_shortcode('[/wpv-for-each]') ;
        ?>
    </div>
</section> 
<?php } ?>

<?php if ( get_post_meta($post->ID, $intPageContent."-".$lang, true)) { ?>
<section id="loveContent" class="details overlapPanel">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">
            <div class="contentDetail col">
                <?php
                    echo do_shortcode('[types field="'.ltrim($intPageContent,"wpcf-")."-".$lang.'" output="raw"]') ;
                ?>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<?php if ( get_post_meta($post->ID, $intImages, true)) { ?>
<section id="sliderInterior"  class="slider">
    <div class="bxslider text-center w-100">
        <?php
            do_shortcode('[wpv-for-each field="'.$intImages.'"]') ;
                echo do_shortcode('[types field="'.ltrim($intImages,"wpcf-").'"]');
            do_shortcode('[/wpv-for-each]') ;
        ?>
    </div>
</section> 
<?php } ?>

<?php
    $loop = new WP_Query( 'page_id=14' );

    if ( $loop->have_posts() ) : 

        while ($loop->have_posts()) : $loop->the_post();     
?>
<section id="getInTouch" class="panel" style="background-image: linear-gradient(rgba(34, 34, 34, 0.7),rgba(34, 34, 34, 0.7)),url(<?php if ( get_post_meta($post->ID, $background, true)) { echo do_shortcode('[types field="set-background-image" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">    
            <div class="col-lg-3 col-sm-12" data-5000="filter:blur(10px);transform: translate(0, 0px);" data-5200="filter:blur(0px)" data-6000="filter:blur(10px);transform: translate(0, -100px);"> <!-- data-9600="filter:blur(2px)" data-10000="filter:blur(0px)" data-11000="filter:blur(10px)" -->
                <a href="<?php if ( get_post_meta($post->ID, $link, true)) { echo do_shortcode('[types field="link-to-page" output="raw"]'); } else { echo "#"; } ?>">
                    <h3>
                        <?php
                            if ( get_post_meta($post->ID, $gitHeading."-".$lang, true)) echo do_shortcode('[types field="'.ltrim($gitHeading,"wpcf-")."-".$lang.'"]');
                            if ( get_post_meta($post->ID, $gitSubHeading."-".$lang, true)) echo "<span>" . do_shortcode('[types field="'.ltrim($gitSubHeading,"wpcf-")."-".$lang.'"]') . "</span>" ;
                        ?>
                    </h3>

                    <h2>
                        <?php
                            if ( get_post_meta($post->ID, $addheading."-".$lang, true)) echo  do_shortcode('[types field="'.ltrim($addheading,"wpcf-")."-".$lang.'"]');
                        ?> &raquo;
                    </h2>
                </a>
            </div>
        </div>
    </div>
</section>
<?php 
        endwhile; wp_reset_query(); 
    endif;
?>

<?php endwhile; wp_reset_query(); ?>

<?php get_footer(); ?>