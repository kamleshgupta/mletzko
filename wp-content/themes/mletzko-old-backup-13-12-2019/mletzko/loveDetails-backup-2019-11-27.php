<?php
    /* Template Name: Love Details */ 

    get_header(); 
    $template_url = get_bloginfo('template_url');

    $lang = "en";
    
    // Section First
    $secFirBackground = "wpcf-sec-fir-background";
    $secFirCFirst = "wpcf-sec1-string-first";
    $secFirCSecond = "wpcf-sec1-string-second";
    $secFirCThird = "wpcf-sec1-string-third";


    // Section Second
    $secSecBackground = "wpcf-sec-second-background";
    $secSecCFirst = "wpcf-sec2-string-first";
    $secSecCSecond = "wpcf-sec2-string-second";


    // Section Third
    $secThirdBackground = "wpcf-sec-third-background";
    $secThirdCFirst = "wpcf-sec3-string-first";
    $secThirdCSecond = "wpcf-sec3-string-second";


    // Section Fourth
    $secFourBackground = "wpcf-sec-fourth-background";
    $secFourCFirst = "wpcf-sec4-string-first";
    $secFourCSecond = "wpcf-sec4-string-second";


    // Section Fifth
    $secFiveBackground = "wpcf-sec-fifth-background";
    $secFiveCFirst = "wpcf-sec5-string-first";
    $secFiveCSecond = "wpcf-sec5-string-second";


    // Section Sixth
    $secSixBackground = "wpcf-sec-sixth-background";
    $secSixCFirst = "wpcf-sec6-string-first";
    $secSixCSecond = "wpcf-sec6-string-second";


    // Section Seventh
    $secSevenBackground = "wpcf-sec-seven-background";
    $secSevenCFirst = "wpcf-sec7-string-first";
    $secSevenCSecond = "wpcf-sec7-string-second";


    // Section Eighth
    $secEightBackground = "wpcf-sec-eight-background";
    $secEightCFirst = "wpcf-sec8-string-first";
    $secEightCSecond = "wpcf-sec8-string-second";


    // Section Nineth
    $secNineBackground = "wpcf-sec-nine-background";
    $secNineCFirst = "wpcf-sec9-string-first";
    $secNineCSecond = "wpcf-sec9-string-second";


    // Section Tenth
    $secTenBackground = "wpcf-sec-ten-background";
    $secTenCFirst = "wpcf-sec10-string-first";
    $secTenCSecond = "wpcf-sec10-string-second";

    // Get In Touch Fields
    $background = "wpcf-set-background-image";
    $link = "wpcf-link-to-page";
    $gitHeading = "wpcf-git-heading";
    $gitSubHeading = "wpcf-git-sub-heading";
    $addheading = "wpcf-additional-heading";

    // Page Content
    $pageContent = "wpcf-spage-content";
    global $post; 
?>
<?php while ( have_posts() ): the_post(); ?>

<div id="detailsOpacity" style="background-image: url(<?php if ( get_post_meta($post->ID, $secFirBackground, true)) { echo do_shortcode('[types field="'.ltrim($secFirBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);"></div>
<section id="heartbeat">
    <div class="container">
        <div class="row align-items-center text-left">    
            <div data-0="opacity: 1;" data-400="opacity: 0.5;" class="col smallDev">
                <?php
                    if ( get_post_meta($post->ID, $secFirCFirst."-".$lang, true)) echo "<h3>" . do_shortcode('[types field="'.ltrim($secFirCFirst,"wpcf-")."-".$lang.'"]') . "</h3>" ;
                    if ( get_post_meta($post->ID, $secFirCSecond."-".$lang, true)) echo "<h4>" . do_shortcode('[types field="'.ltrim($secFirCSecond,"wpcf-")."-".$lang.'"]') . "</h4>" ;
                    if ( get_post_meta($post->ID, $secFirCThird."-".$lang, true)) echo "<h2>" . do_shortcode('[types field="'.ltrim($secFirCThird,"wpcf-")."-".$lang.'"]') . "</h2>" ;
                ?>
            </div>
        </div>        
    </div>
</section>

<section id="fillerFlap" style="background-image: url(<?php if ( get_post_meta($post->ID, $secSecBackground, true)) { echo do_shortcode('[types field="'.ltrim($secSecBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container">
        <div class="row align-items-center">    
            <div data-600="opacity: 0.5;" data-800="opacity: 1.5;"  data-1200="opacity: 0.5;" class="col-lg-11 text-center text-lg-right p-2">
                <?php
                    if ( get_post_meta($post->ID, $secSecCFirst."-".$lang, true)) echo "<h3>" . do_shortcode('[types field="'.ltrim($secSecCFirst,"wpcf-")."-".$lang.'"]') . "</h3>" ;
                    if ( get_post_meta($post->ID, $secSecCSecond."-".$lang, true)) echo "<h4>" . do_shortcode('[types field="'.ltrim($secSecCSecond,"wpcf-")."-".$lang.'"]') . "</h4>" ;
                ?>
            </div>
        </div>
    </div>
</section>

<section id="outsideMirror" style="background-image: url(<?php if ( get_post_meta($post->ID, $secThirdBackground, true)) { echo do_shortcode('[types field="'.ltrim($secThirdBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container">
        <div class="row align-items-center">    
            <div data-1400="opacity: 0.5;" data-1600="opacity: 1.5;"  data-2200="opacity: 0.5;" class="col-lg-4">
                <?php
                    echo "<h3>";
                    if ( get_post_meta($post->ID, $secThirdCFirst."-".$lang, true)) echo do_shortcode('[types field="'.ltrim($secThirdCFirst,"wpcf-")."-".$lang.'"]') ;
                    if ( get_post_meta($post->ID, $secThirdCSecond."-".$lang, true)) echo "<span>" . do_shortcode('[types field="'.ltrim($secThirdCSecond,"wpcf-")."-".$lang.'"]') . "</span>" ;
                    echo "</h3>";
                ?>
            </div>
        </div>
    </div>
</section>

<section id="headlights" style="background-image: url(<?php if ( get_post_meta($post->ID, $secFourBackground, true)) { echo do_shortcode('[types field="'.ltrim($secFourBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container">
        <div class="row align-items-center">    
            <div data-2400="opacity: 0.5;" data-2600="opacity: 1.5;"  data-3200="opacity: 0.5;"  class="col-lg-11 text-center text-lg-right p-2">                   
                <?php
                    if ( get_post_meta($post->ID, $secFourCFirst."-".$lang, true)) echo "<h3>" . do_shortcode('[types field="'.ltrim($secFourCFirst,"wpcf-")."-".$lang.'"]') . "</h3>" ;
                    if ( get_post_meta($post->ID, $secFourCSecond."-".$lang, true)) echo "<h4>" . do_shortcode('[types field="'.ltrim($secFourCSecond,"wpcf-")."-".$lang.'"]') . "</h4>" ;
                ?>
            </div>
        </div>
    </div>
</section>

<section id="handles" style="background-image: url(<?php if ( get_post_meta($post->ID, $secFiveBackground, true)) { echo do_shortcode('[types field="'.ltrim($secFiveBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container">
        <div class="row align-items-center">    
            <div data-3400="opacity: 0.5;" data-3600="opacity: 1.5;"  data-4200="opacity: 0.5;"  class="col-lg-5">
                <?php
                    echo "<h3>";
                    if ( get_post_meta($post->ID, $secFiveCFirst."-".$lang, true)) echo do_shortcode('[types field="'.ltrim($secFiveCFirst,"wpcf-")."-".$lang.'"]') ;
                    if ( get_post_meta($post->ID, $secFiveCSecond."-".$lang, true)) echo "<span>" . do_shortcode('[types field="'.ltrim($secFiveCSecond,"wpcf-")."-".$lang.'"]') . "</span>" ;
                    echo "</h3>";
                ?>
            </div>
        </div>
    </div>
</section>

<section id="rims" style="background-image: url(<?php if ( get_post_meta($post->ID, $secSixBackground, true)) { echo do_shortcode('[types field="'.ltrim($secSixBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container">
        <div class="row align-items-center">    
            <div data-4200="opacity: 0.5;" data-4400="opacity: 1.5;"  data-5000="opacity: 0.5;"  class="col-lg-11 text-center text-lg-right p-2">
                <?php
                    if ( get_post_meta($post->ID, $secSixCFirst."-".$lang, true)) echo "<h3>" . do_shortcode('[types field="'.ltrim($secSixCFirst,"wpcf-")."-".$lang.'"]') . "</h3>" ;
                    if ( get_post_meta($post->ID, $secSixCSecond."-".$lang, true)) echo "<h4>" . do_shortcode('[types field="'.ltrim($secSixCSecond,"wpcf-")."-".$lang.'"]') . "</h4>" ;
                ?>
            </div>
        </div>
    </div>
</section>

<section id="bodyhr" style="background-image: url(<?php if ( get_post_meta($post->ID, $secSevenBackground, true)) { echo do_shortcode('[types field="'.ltrim($secSevenBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container">
        <div class="row align-items-center">    
            <div data-5000="opacity: 0.5;" data-5200="opacity: 1.5;"  data-6000="opacity: 0.5;" class="col-lg-12">
                <?php
                    echo "<h3>";
                    if ( get_post_meta($post->ID, $secSevenCFirst."-".$lang, true)) echo do_shortcode('[types field="'.ltrim($secSevenCFirst,"wpcf-")."-".$lang.'"]') ;
                    if ( get_post_meta($post->ID, $secSevenCSecond."-".$lang, true)) echo "<span>" . do_shortcode('[types field="'.ltrim($secSevenCSecond,"wpcf-")."-".$lang.'"]') . "</span>" ;
                    echo "</h3>";
                ?>
            </div>
        </div>
    </div>
</section>

<section id="engine" style="background-image: url(<?php if ( get_post_meta($post->ID, $secEightBackground, true)) { echo do_shortcode('[types field="'.ltrim($secEightBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container">
        <div class="row align-items-center">    
            <div data-6000="opacity: 0.5;" data-6200="opacity: 1.5;"  data-7000="opacity: 0.5;" class="col-lg-4">
                <?php
                    echo "<h3>";
                    if ( get_post_meta($post->ID, $secEightCFirst."-".$lang, true)) echo do_shortcode('[types field="'.ltrim($secEightCFirst,"wpcf-")."-".$lang.'"]') ;
                    if ( get_post_meta($post->ID, $secEightCSecond."-".$lang, true)) echo "<span>" . do_shortcode('[types field="'.ltrim($secEightCSecond,"wpcf-")."-".$lang.'"]') . "</span>" ;
                    echo "</h3>";
                ?>
            </div>
        </div>
    </div>
</section>

<section id="cockpit" style="background-image: url(<?php if ( get_post_meta($post->ID, $secNineBackground, true)) { echo do_shortcode('[types field="'.ltrim($secNineBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container">
        <div class="row align-items-center">    
            <div data-7000="opacity: 0.5;" data-7200="opacity: 1.5;"  data-8000="opacity: 0.5;" class="col-lg-11 text-center text-lg-right p-2">
                <?php
                    if ( get_post_meta($post->ID, $secNineCFirst."-".$lang, true)) echo "<h3>" . do_shortcode('[types field="'.ltrim($secNineCFirst,"wpcf-")."-".$lang.'"]') . "</h3>" ;
                    if ( get_post_meta($post->ID, $secNineCSecond."-".$lang, true)) echo "<h4>" . do_shortcode('[types field="'.ltrim($secNineCSecond,"wpcf-")."-".$lang.'"]') . "</h4>" ;
                ?>
            </div>
        </div>
    </div>
</section>

<section id="doorPin" style="background-image: url(<?php if ( get_post_meta($post->ID, $secTenBackground, true)) { echo do_shortcode('[types field="'.ltrim($secTenBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container">
        <div class="row align-items-center">    
            <div class="col-lg-5">
                <?php
                    echo "<h3>";
                    if ( get_post_meta($post->ID, $secTenCFirst."-".$lang, true)) echo do_shortcode('[types field="'.ltrim($secTenCFirst,"wpcf-")."-".$lang.'"]') ;
                    if ( get_post_meta($post->ID, $secTenCSecond."-".$lang, true)) echo "<span>" . do_shortcode('[types field="'.ltrim($secTenCSecond,"wpcf-")."-".$lang.'"]') . "</span>" ;
                    echo "</h3>";
                ?>
            </div>
        </div>
    </div>
</section>

<?php if ( get_post_meta($post->ID, $pageContent."-".$lang, true)) { ?>
<section id="loveContent">
    <div class="container">
        <div class="row">
            <div class="col">
                <div data-9000="opacity: 0.5;" data-9200="opacity: 1.5;"  data-10000="opacity: 0.5;" class="contentDetail text-center w-100">

                    <?php
                        echo do_shortcode('[types field="'.ltrim($pageContent,"wpcf-")."-".$lang.'" output="raw"]') ;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<?php
    $loop = new WP_Query( 'page_id=14' );

    if ( $loop->have_posts() ) : 

        while ($loop->have_posts()) : $loop->the_post();     
?>
<section id="getInTouch" style="background-image: linear-gradient(rgba(34, 34, 34, 0.7),rgba(34, 34, 34, 0.7)),url(<?php if ( get_post_meta($post->ID, $background, true)) { echo do_shortcode('[types field="set-background-image" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container">
        <div class="row align-items-center justify-content-center">    
            <div class="p-0" data-9900="opacity: 0.5;" data-10100="opacity: 1.5;" data-10400="opacity: 0.5;">
                <a href="<?php if ( get_post_meta($post->ID, $link, true)) { echo do_shortcode('[types field="link-to-page" output="raw"]'); } else { echo "#"; } ?>">
                    <h3>
                        <?php
                            if ( get_post_meta($post->ID, $gitHeading."-".$lang, true)) echo do_shortcode('[types field="'.ltrim($gitHeading,"wpcf-")."-".$lang.'"]');
                            if ( get_post_meta($post->ID, $gitSubHeading."-".$lang, true)) echo "<span>" . do_shortcode('[types field="'.ltrim($gitSubHeading,"wpcf-")."-".$lang.'"]') . "</span>" ;
                        ?>
                    </h3>

                    <h2>
                        <?php
                            if ( get_post_meta($post->ID, $addheading."-".$lang, true)) echo  do_shortcode('[types field="'.ltrim($addheading,"wpcf-")."-".$lang.'"]');
                        ?> &raquo;
                    </h2>
                </a>
            </div>
        </div>
    </div>
</section>
<?php 
        endwhile; wp_reset_query(); 
    endif;
?>

<?php endwhile; wp_reset_query(); ?>

<?php get_footer(); ?>