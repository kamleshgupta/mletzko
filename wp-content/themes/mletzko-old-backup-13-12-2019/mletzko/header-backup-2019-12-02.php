<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Mletzko
 * @since Mletzko 1.0
 */
$selected_language = $_COOKIE['selected_language'] ? $_COOKIE['selected_language'] : 'EN';
$template_url = get_template_directory_uri();
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>">
	<?php endif; ?>
	
	<?php wp_head(); ?>

	<!-- Website Specific Metas -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Favicons -->
	<link type="image/x-icon" rel="shortcut icon" href="<?php echo $template_url; ?>/images/favicon.png" />
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<header id="masthead" class="site-header p-0 w-100 position-fixed">
		<div id="myNav" class="overlay">
			<div class="container-fluid">
				<div class="row justify-content-end">
					<div class="col-lg-2 text-right mt-lg-1">
						<ul class="hamburger">
							<li>
								<a href="javascript:void(0)" class="text-uppercase language_selecter"><?php echo $_COOKIE['lang']; ?></a>
							</li>
							<li>
								<a href="javascript:void(0)" class="closebtn" id="closeNav">
									<img src="<?php echo $template_url; ?>/images/close.svg" alt="Close">
								</a>
							</li>
						</ul>
					</div>
					
				</div>
			
				<div class="overlay-content">
					<div class="row">
						<div class="col-12">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
								<img id="menuPopupLogo" src="<?php echo get_template_directory_uri(); ?>'/images/logo.svg'" class="img-fluid" alt="<?php bloginfo( 'name' ); ?>" />
							</a>
						</div>
						<div class="col-12 justify-content-end">
							<?php if ( has_nav_menu( 'primary' ) ) : ?>
								<div class="row align-auto">
									<div class="col-12">
										<?php
											$lang = strtolower($_COOKIE['lang']); //"en";
											// echo $lang;

											if ($lang == "en") {
												wp_nav_menu(
													array(
														'theme_location'  => 'primary',
														'menu' 			  => 'primary',
														'container'       => 'ul',
														'menu_class'      => 'primary-menu',
													)
												);						
											} else {
												wp_nav_menu(
													array(
														'theme_location'  => 'primaryDe',
														'menu' 			  => 'primaryDe',
														'container'       => 'ul',
														'menu_class'      => 'primary-menu',
													)
												);		
											}											
										?>
									</div>
								</div>	
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-6 col-md-6 col-lg-6">
					<h1><?php mletzko_the_custom_logo(); ?></h1>
				</div>
				<div class="col-xs-6  col-sm-6 col-md-6 col-lg-6 text-right">
					<span id="openNav"><img src="<?php echo $template_url; ?>/images/hamburger.svg" alt="Menu"></span>
				</div>
			</div>
		</div>
		
	</header><!-- .site-header -->
	
	<div id="content" class="site-content p-0">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/skrollr/0.6.30/skrollr.min.js"></script>
<script>
var width = (window.innerWidth > 0) ? window.innerWidth : document.documentElement.clientWidth;
        if(width > 992){
          $(function(){
            skrollr.init({
            forceHeight: false
          });
        });
        } 
</script>

<script type="text/javascript">
	(function(){
		var home_url = '<?php echo get_home_url();?>';
		jQuery('#mletzko_site').attr('href', home_url);
	});
</script>