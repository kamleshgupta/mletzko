<?php
    /**
     * The main template file
     *
     * This is the most generic template file in a WordPress theme
     * and one of the two required files for a theme (the other being style.css).
     * It is used to display a page when nothing more specific matches a query.
     * E.g., it puts together the home page when no home.php file exists.
     *
     * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
     *
     * @package WordPress
     * @subpackage Mletzko
     * @since Mletzko 1.0
    */

    get_header();

    $lang = strtolower($_COOKIE['lang']); //"en";
    $heading = "wpcf-heading";
    $subHeading = "wpcf-sub-heading";

    $pageContent = "wpcf-spage-content";

?>

<?php while ( have_posts() ): the_post(); ?>
    <section id="defaultContent">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-center">
                <div class="col-xl-8 col-lg-9">
                    <h2 class='entry-title'>
                        <?php 
                            if ( get_post_meta($post->ID, $heading."-".$lang, true)) {
                                echo  do_shortcode('[types field="'.ltrim($heading,"wpcf-")."-".$lang.'"]');

                                if ( get_post_meta($post->ID, $subHeading."-".$lang, true)) { 
                                    echo "<span>" . do_shortcode('[types field="'.ltrim($subHeading,"wpcf-")."-".$lang.'"]') . "</span>";
                                }
                            } else { 
                                the_title(); 
                            }
                        ?>
                    </h2>

                    <div class='entry-page-content'>
                        <?php
                            if ( get_post_meta($post->ID, $pageContent."-".$lang, true)) {
                                echo do_shortcode('[types field="'.ltrim($pageContent,"wpcf-")."-".$lang.'"]');
                            } else {
                                the_content();
                            }
                        ?>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
<?php endwhile; wp_reset_query(); ?> 

<?php get_footer(); ?>
