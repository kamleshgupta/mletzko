<?php
    /* Template Name: Love Details */ 

    get_header(); 
    $template_url = get_bloginfo('template_url');

    $lang = strtolower($_COOKIE['lang']); //"en";
    
    // Section First
    $secFirBackground = "wpcf-sec-fir-background";
    $secFirCFirst = "wpcf-sec1-string-first";
    $secFirCSecond = "wpcf-sec1-string-second";
    $secFirCThird = "wpcf-sec1-string-third";


    // Section Second
    $secSecBackground = "wpcf-sec-second-background";
    $secSecCFirst = "wpcf-sec2-string-first";
    $secSecCSecond = "wpcf-sec2-string-second";


    // Section Third
    $secThirdBackground = "wpcf-sec-third-background";
    $secThirdCFirst = "wpcf-sec3-string-first";
    $secThirdCSecond = "wpcf-sec3-string-second";


    // Section Fourth
    $secFourBackground = "wpcf-sec-fourth-background";
    $secFourCFirst = "wpcf-sec4-string-first";
    $secFourCSecond = "wpcf-sec4-string-second";


    // Section Fifth
    $secFiveBackground = "wpcf-sec-fifth-background";
    $secFiveCFirst = "wpcf-sec5-string-first";
    $secFiveCSecond = "wpcf-sec5-string-second";


    // Section Sixth
    $secSixBackground = "wpcf-sec-sixth-background";
    $secSixCFirst = "wpcf-sec6-string-first";
    $secSixCSecond = "wpcf-sec6-string-second";


    // Section Seventh
    $secSevenBackground = "wpcf-sec-seven-background";
    $secSevenCFirst = "wpcf-sec7-string-first";
    $secSevenCSecond = "wpcf-sec7-string-second";


    // Section Eighth
    $secEightBackground = "wpcf-sec-eight-background";
    $secEightCFirst = "wpcf-sec8-string-first";
    $secEightCSecond = "wpcf-sec8-string-second";


    // Section Nineth
    $secNineBackground = "wpcf-sec-nine-background";
    $secNineCFirst = "wpcf-sec9-string-first";
    $secNineCSecond = "wpcf-sec9-string-second";


    // Section Tenth
    $secTenBackground = "wpcf-sec-ten-background";
    $secTenCFirst = "wpcf-sec10-string-first";
    $secTenCSecond = "wpcf-sec10-string-second";

    // Get In Touch Fields
    $background = "wpcf-set-background-image";
    $link = "wpcf-link-to-page";
    $gitHeading = "wpcf-git-heading";
    $gitSubHeading = "wpcf-git-sub-heading";
    $addheading = "wpcf-additional-heading";

    // Page Content
    $pageContent = "wpcf-spage-content";
    global $post; 
?>
<?php while ( have_posts() ): the_post(); ?>

<div class="overlapPanel" style="background-image: url(<?php if ( get_post_meta($post->ID, $secFirBackground, true)) { echo do_shortcode('[types field="'.ltrim($secFirBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);"></div>
<section id="heartbeat" class="overlapPanel bannerArea">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-start">    
            <div data-0="opacity: 1;filter:blur(0px);transform: translate(0, 0px);transition-duration: 3s linear;" data-400="opacity: 1;filter:blur(10px);transform: translate(0, -550px);transition-duration: 3s linear;" class="col-lg-7 col-sm-12 smallDev">
                <?php 
                    if ( get_post_meta($post->ID, $secFirCFirst."-".$lang, true)) {
                        echo  "<h3>" . do_shortcode('[types field="'.ltrim($secFirCFirst,"wpcf-")."-".$lang.'"]');

                        if ( get_post_meta($post->ID, $secFirCSecond."-".$lang, true)) { 
                            echo "<span>" . do_shortcode('[types field="'.ltrim($secFirCSecond,"wpcf-")."-".$lang.'"]') . "</span>";
                        }

                        echo "</h3>";
                    }

                    if ( get_post_meta($post->ID, $secFirCThird."-".$lang, true)) {
                        echo "<h4>" . do_shortcode('[types field="'.ltrim($secFirCThird,"wpcf-")."-".$lang.'"]') . "</h4>";
                    }
                ?>
                
            </div>
        </div>     
    </div>
</section>

<section id="fillerFlap" class="overlapPanel panel" style="background-image: url(<?php if ( get_post_meta($post->ID, $secSecBackground, true)) { echo do_shortcode('[types field="'.ltrim($secSecBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-end">    
            <div data-900="filter:blur(10px);transform: translate(0, 0px);transition-duration: 3s linear;" data-1000="filter:blur(0px)"  data-1400="filter:blur(10px);transform: translate(0, -550px);transition-duration: 3s linear" class="col-lg-4 col-md-6 col-sm-12 mr-lg-3 mx-sm-0 ml-0">
                <?php 
                    if ( get_post_meta($post->ID, $secSecCFirst."-".$lang, true)) {
                        echo  "<h3>" . do_shortcode('[types field="'.ltrim($secSecCFirst,"wpcf-")."-".$lang.'"]');

                        if ( get_post_meta($post->ID, $secSecCSecond."-".$lang, true)) { 
                            echo "<span>" . do_shortcode('[types field="'.ltrim($secSecCSecond,"wpcf-")."-".$lang.'"]') . "</span>";
                        }

                        echo "</h3>";
                    }
                ?>
            </div>
        </div>
    </div>
</section>

<section id="outsideMirror" class="overlapPanel panel" style="background-image: url(<?php if ( get_post_meta($post->ID, $secThirdBackground, true)) { echo do_shortcode('[types field="'.ltrim($secThirdBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-start">    
            <div data-1400="filter:blur(10px);transform: translate(0, 0px);transition-duration: 3s linear;" data-1900="filter:blur(0px)"  data-2400="filter:blur(10px);transform: translate(0, -550px);transition-duration: 3s linear;" class="col-lg-4 col-md-6 col-sm-12 ml-lg-3 mx-sm-0 ml-0">
                <?php 
                    if ( get_post_meta($post->ID, $secThirdCFirst."-".$lang, true)) {
                        echo  "<h3>" . do_shortcode('[types field="'.ltrim($secThirdCFirst,"wpcf-")."-".$lang.'"]');

                        if ( get_post_meta($post->ID, $secThirdCSecond."-".$lang, true)) { 
                            echo "<span>" . do_shortcode('[types field="'.ltrim($secThirdCSecond,"wpcf-")."-".$lang.'"]') . "</span>";
                        }

                        echo "</h3>";
                    }
                ?>
            </div>
        </div>
    </div>
</section>

<section id="headlights" class="overlapPanel panel" style="background-image: url(<?php if ( get_post_meta($post->ID, $secFourBackground, true)) { echo do_shortcode('[types field="'.ltrim($secFourBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-end">    
            <div data-2400="filter:blur(10px);transform: translate(0, 0px);transition-duration: 3s linear;" data-2900="filter:blur(0px)"  data-3400="filter:blur(10px);transform: translate(0, -550px);transition-duration: 3s linear;"  class="col-lg-4 col-md-6 col-sm-12 mr-lg-3 mx-sm-0 ml-0">                   
                <?php 
                    if ( get_post_meta($post->ID, $secFourCFirst."-".$lang, true)) {
                        echo  "<h3>" . do_shortcode('[types field="'.ltrim($secFourCFirst,"wpcf-")."-".$lang.'"]');

                        if ( get_post_meta($post->ID, $secFourCSecond."-".$lang, true)) { 
                            echo "<span>" . do_shortcode('[types field="'.ltrim($secFourCSecond,"wpcf-")."-".$lang.'"]') . "</span>";
                        }

                        echo "</h3>";
                    }
                ?>
            </div>
        </div>
    </div>
</section>

<section id="handles" class="overlapPanel panel" style="background-image: url(<?php if ( get_post_meta($post->ID, $secFiveBackground, true)) { echo do_shortcode('[types field="'.ltrim($secFiveBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-start">    
            <div data-3400="filter:blur(10px);transform: translate(0, 0px);transition-duration: 3s linear;" data-3900="filter:blur(0px)"  data-4400="filter:blur(10px);transform: translate(0, -550px);transition-duration: 5s linear;"  class="col-lg-4 col-md-6 col-sm-12 ml-lg-3 mx-sm-0 ml-0">
                <?php 
                    if ( get_post_meta($post->ID, $secFiveCFirst."-".$lang, true)) {
                        echo  "<h3>" . do_shortcode('[types field="'.ltrim($secFiveCFirst,"wpcf-")."-".$lang.'"]');

                        if ( get_post_meta($post->ID, $secFiveCSecond."-".$lang, true)) { 
                            echo "<span>" . do_shortcode('[types field="'.ltrim($secFiveCSecond,"wpcf-")."-".$lang.'"]') . "</span>";
                        }

                        echo "</h3>";
                    }
                ?>
            </div>
        </div>
    </div>
</section>

<section id="rims" class="overlapPanel panel" style="background-image: url(<?php if ( get_post_meta($post->ID, $secSixBackground, true)) { echo do_shortcode('[types field="'.ltrim($secSixBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-end">    
            <div data-4400="filter:blur(10px);transform: translate(0, 0px);transition-duration: 3s linear;" data-4900="filter:blur(0px)"  data-5400="filter:blur(10px);transform: translate(0, -550px);transition-duration: 3s linear;"  class="col-lg-4 col-md-6 col-sm-12 mr-lg-3 mx-sm-0 ml-0">
                <?php 
                    if ( get_post_meta($post->ID, $secSixCFirst."-".$lang, true)) {
                        echo  "<h3>" . do_shortcode('[types field="'.ltrim($secSixCFirst,"wpcf-")."-".$lang.'"]');

                        if ( get_post_meta($post->ID, $secSixCSecond."-".$lang, true)) { 
                            echo "<span>" . do_shortcode('[types field="'.ltrim($secSixCSecond,"wpcf-")."-".$lang.'"]') . "</span>";
                        }

                        echo "</h3>";
                    }
                ?>
            </div>
        </div>
    </div>
</section>

<section id="bodyhr" class="overlapPanel panel" style="background-image: url(<?php if ( get_post_meta($post->ID, $secSevenBackground, true)) { echo do_shortcode('[types field="'.ltrim($secSevenBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">    
            <div data-5400="filter:blur(10px);transform: translate(0, 0px);transition-duration: 3s linear;" data-5800="filter:blur(0px)"  data-6400="filter:blur(10px);transform: translate(0, -550px);transition-duration: 3s linear;" class="col-lg-4 col-sm-12">
                <?php 
                    if ( get_post_meta($post->ID, $secSevenCFirst."-".$lang, true)) {
                        echo  "<h3>" . do_shortcode('[types field="'.ltrim($secSevenCFirst,"wpcf-")."-".$lang.'"]');

                        if ( get_post_meta($post->ID, $secSevenCSecond."-".$lang, true)) { 
                            echo "<span>" . do_shortcode('[types field="'.ltrim($secSevenCSecond,"wpcf-")."-".$lang.'"]') . "</span>";
                        }

                        echo "</h3>";
                    }
                ?>
            </div>
        </div>
    </div>
</section>

<section id="engine" class="overlapPanel panel" style="background-image: url(<?php if ( get_post_meta($post->ID, $secEightBackground, true)) { echo do_shortcode('[types field="'.ltrim($secEightBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-start">    
            <div data-6400="filter:blur(10px);transform: translate(0, 0px);transition-duration: 3s linear;" data-6900="filter:blur(0px)"  data-7400="filter:blur(10px);transform: translate(0, -550px);transition-duration: 3s linear;" class="col-lg-4 col-md-6 col-sm-12 ml-lg-3 mx-sm-0 ml-0">
                <?php 
                    if ( get_post_meta($post->ID, $secEightCFirst."-".$lang, true)) {
                        echo  "<h3>" . do_shortcode('[types field="'.ltrim($secEightCFirst,"wpcf-")."-".$lang.'"]');

                        if ( get_post_meta($post->ID, $secEightCSecond."-".$lang, true)) { 
                            echo "<span>" . do_shortcode('[types field="'.ltrim($secEightCSecond,"wpcf-")."-".$lang.'"]') . "</span>";
                        }

                        echo "</h3>";
                    }
                ?>
            </div>
        </div>
    </div>
</section>

<section id="cockpit" class="overlapPanel panel" style="background-image: url(<?php if ( get_post_meta($post->ID, $secNineBackground, true)) { echo do_shortcode('[types field="'.ltrim($secNineBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-end">    
            <div data-7400="filter:blur(10px);transform: translate(0, 0px);transition-duration: 3s linear;" data-7900="filter:blur(0px)"  data-8400="filter:blur(10px);transform: translate(0, -550px);transition-duration: 3s linear;" class="col-lg-4 col-md-6 col-sm-12 mr-lg-3 mx-sm-0 ml-0">
                <?php 
                    if ( get_post_meta($post->ID, $secNineCFirst."-".$lang, true)) {
                        echo  "<h3>" . do_shortcode('[types field="'.ltrim($secNineCFirst,"wpcf-")."-".$lang.'"]');

                        if ( get_post_meta($post->ID, $secNineCSecond."-".$lang, true)) { 
                            echo "<span>" . do_shortcode('[types field="'.ltrim($secNineCSecond,"wpcf-")."-".$lang.'"]') . "</span>";
                        }

                        echo "</h3>";
                    }
                ?>
            </div>
        </div>
    </div>
</section>

<section id="doorPin" class="overlapPanel panel" style="background-image: url(<?php if ( get_post_meta($post->ID, $secTenBackground, true)) { echo do_shortcode('[types field="'.ltrim($secTenBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-start">    
            <div data-8400="filter:blur(10px);transform: translate(0, 0px);transition-duration: 3s linear;" data-8900="filter:blur(0px)"  data-9400="filter:blur(10px);transform: translate(0, -550px);transition-duration: 3s linear;" class="col-lg-4 col-md-6 col-sm-12 ml-lg-3 mx-sm-0 ml-0">
                <?php 
                    if ( get_post_meta($post->ID, $secTenCFirst."-".$lang, true)) {
                        echo  "<h3>" . do_shortcode('[types field="'.ltrim($secTenCFirst,"wpcf-")."-".$lang.'"]');

                        if ( get_post_meta($post->ID, $secTenCSecond."-".$lang, true)) { 
                            echo "<span>" . do_shortcode('[types field="'.ltrim($secTenCSecond,"wpcf-")."-".$lang.'"]') . "</span>";
                        }

                        echo "</h3>";
                    }
                ?>
            </div>
        </div>
    </div>
</section>

<?php if ( get_post_meta($post->ID, $pageContent."-".$lang, true)) { ?>
<section id="loveContent" class="details overlapPanel panel">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">
            <div class="contentDetail col">
                
                    <?php
                        echo do_shortcode('[types field="'.ltrim($pageContent,"wpcf-")."-".$lang.'" output="raw"]') ;
                    ?>
                
            </div>
        </div>
    </div>
</section>
<?php } ?>

<?php
    $loop = new WP_Query( 'page_id=14' );

    if ( $loop->have_posts() ) : 

        while ($loop->have_posts()) : $loop->the_post();     
?>
<section id="getInTouch" class="panel" style="background-image: linear-gradient(rgba(34, 34, 34, 0.7),rgba(34, 34, 34, 0.7)),url(<?php if ( get_post_meta($post->ID, $background, true)) { echo do_shortcode('[types field="set-background-image" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">    
            <div class="col-lg-3 col-sm-12" data-9600="filter:blur(10px);transform: translate(0, 0px);transition-duration: 3s linear;" data-10000="filter:blur(0px);" data-11000="filter:blur(10px);transform: translate(0, -550px);transition-duration: 3s linear;"> <!-- data-9600="filter:blur(2px)" data-10000="filter:blur(0px)" data-11000="filter:blur(5px)" -->
                <a href="<?php if ( get_post_meta($post->ID, $link, true)) { echo do_shortcode('[types field="link-to-page" output="raw"]'); } else { echo "#"; } ?>">
                    <h3>
                        <?php
                            if ( get_post_meta($post->ID, $gitHeading."-".$lang, true)) echo do_shortcode('[types field="'.ltrim($gitHeading,"wpcf-")."-".$lang.'"]');
                            if ( get_post_meta($post->ID, $gitSubHeading."-".$lang, true)) echo "<span>" . do_shortcode('[types field="'.ltrim($gitSubHeading,"wpcf-")."-".$lang.'"]') . "</span>" ;
                        ?>
                    </h3>

                    <h2>
                        <?php
                            if ( get_post_meta($post->ID, $addheading."-".$lang, true)) echo  do_shortcode('[types field="'.ltrim($addheading,"wpcf-")."-".$lang.'"]');
                        ?> &raquo;
                    </h2>
                </a>
            </div>
        </div>
    </div>
</section>
<?php 
        endwhile; wp_reset_query(); 
    endif;
?>

<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri() . '/js/debug.addIndicators.min.js' ?>"></script>
<script>
    var width = (window.innerWidth > 0) ? window.innerWidth : document.documentElement.clientWidth;
        if(width > 992){

            $(function() { // wait for document ready
    // init
    var flag = false;
    var controller = new ScrollMagic.Controller({
        globalSceneOptions: {
            triggerHook: 'onLeave',
            duration: "0%"
        }
    });

    // get all slides
    var slides = document.querySelectorAll("section.panel");
    console.log(slides.length)
    // create scene for every slide
    for (var i = 0; i < slides.length; i++) {
        new ScrollMagic.Scene({
                triggerElement: slides[i]
            })
            .setPin(slides[i], {
                pushFollowers: false
            })
            .addIndicators() // add indicators (requires plugin)
            .addTo(controller)
    }
    $(window).scroll(function(event) {
        var scroll = $(window).scrollTop();
        if (scroll > 9367) {
            controller.enabled(false);
            flag = true;
        } else if (scroll < 9367 && flag) {
            controller = new ScrollMagic.Controller({
                globalSceneOptions: {
                    triggerHook: 'onLeave',
                    duration: "0%"
                }
            });
            var slides = document.querySelectorAll("section.panel");
            for (var i = 0; i < slides.length; i++) {
                new ScrollMagic.Scene({
                        triggerElement: slides[i]
                    })
                    .setPin(slides[i], {
                        pushFollowers: false
                    })
                    .addIndicators() // add indicators (requires plugin)
                    .addTo(controller)
            }
            flag=false;
        }

    });
});

        }
   
</script>


<?php endwhile; wp_reset_query(); ?>

<?php get_footer(); ?>