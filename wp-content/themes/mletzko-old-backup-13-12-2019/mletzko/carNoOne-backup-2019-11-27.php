<?php
  /* Template Name: Car No 1 */ 

    get_header();
    
    $lang = "en";
    
    // Section First
    $bannerBackground = "wpcf-banner-background";
    $bannerCFirst = "wpcf-banner-content-first";
    $bannerCSecond = "wpcf-banner-content-second";
    $bannerCThird = "wpcf-banner-content-third";

    // Exterior Content
    $extPageContent = "wpcf-exterior-page-content";
    $extImages = "wpcf-exterior-slider-images";

    // Interior Content
    $intPageContent = "wpcf-interior-page-content";
    $intImages = "wpcf-interior-slider-images";
    

    // Get In Touch Fields
    $background = "wpcf-set-background-image";
    $link = "wpcf-link-to-page";
    $gitHeading = "wpcf-git-heading";
    $gitSubHeading = "wpcf-git-sub-heading";
    $addheading = "wpcf-additional-heading";    

    global $post; 
?>
<?php while ( have_posts() ): the_post(); ?>
<!-- Start Your Coding After this Line -->
<div class="carNumOne">
    <div class="bannerCanNoOneBackground" style="background-image: url(<?php if ( get_post_meta($post->ID, $bannerBackground, true)) { echo do_shortcode('[types field="'.ltrim($bannerBackground, "wpcf-").'" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);"></div>
    <section id="bannerCanNoOne" class="backgroundOpacity">
        <div class="container">
            <div data-0="opacity: 1;" data-400="opacity: 0.5;" class="row align-items-center text-left">    
                <div class="col">
                    <?php
                        if ( get_post_meta($post->ID, $bannerCFirst."-".$lang, true)) echo "<h3>" . do_shortcode('[types field="'.ltrim($bannerCFirst,"wpcf-")."-".$lang.'"]') . "</h3>" ;
                        if ( get_post_meta($post->ID, $bannerCSecond."-".$lang, true)) echo "<h4>" . do_shortcode('[types field="'.ltrim($bannerCSecond,"wpcf-")."-".$lang.'"]') . "</h4>" ;
                        if ( get_post_meta($post->ID, $bannerCThird."-".$lang, true)) echo "<p>" . do_shortcode('[types field="'.ltrim($bannerCThird,"wpcf-")."-".$lang.'"]') . "</p>" ;
                    ?>
                </div>
            </div>
        </div>
    </section>

    <?php if ( get_post_meta($post->ID, $extPageContent."-".$lang, true)) { ?>
    <section id="exteriorDetails" class="details">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div  class="interiorDetails text-center w-100">
                        <?php
                            echo do_shortcode('[types field="'.ltrim($extPageContent,"wpcf-")."-".$lang.'" output="raw"]') ;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>

    <?php if ( get_post_meta($post->ID, $extImages, true)) { ?>
    <section id="sliderExterior" class="slider">
		<div class="container_fluid">
		<div class="row m-0">
            <div class="col p-0">
                <div class="bxslider text-center w-100">
                    <?php
                        do_shortcode('[wpv-for-each field="'.$extImages.'"]') ;
                            echo do_shortcode('[types field="'.ltrim($extImages,"wpcf-").'"]');
                        do_shortcode('[/wpv-for-each]') ;
                    ?>
                </div>
            </div>
        </div>

		</div>
        
    </section> 
    <?php } ?>

    <?php if ( get_post_meta($post->ID, $intPageContent."-".$lang, true)) { ?>
    <section id="interiorDetails" class="details">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div  class="interiorDetails text-center w-100">
                        <?php
                            echo do_shortcode('[types field="'.ltrim($intPageContent,"wpcf-")."-".$lang.'" output="raw"]') ;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>

    <?php if ( get_post_meta($post->ID, $intImages, true)) { ?>
    <section id=""  class="slider">
		<div class="container_fluid">
        <div class="row m-0">
            <div class="col p-0">
                <div class="bxslider text-center w-100">
                    <?php
                        do_shortcode('[wpv-for-each field="'.$intImages.'"]') ;
                            echo do_shortcode('[types field="'.ltrim($intImages,"wpcf-").'"]');
                        do_shortcode('[/wpv-for-each]') ;
                    ?>
                </div>
            </div>
		</div>
		</div>
    </section> 
    <?php } ?>
</div>

<?php
    $loop = new WP_Query( 'page_id=14' );

    if ( $loop->have_posts() ) : 

        while ($loop->have_posts()) : $loop->the_post();     
?>   
<section id="getInTouch" style="background-image: linear-gradient(rgba(34, 34, 34, 0.7),rgba(34, 34, 34, 0.7)),url(<?php if ( get_post_meta($post->ID, $background, true)) { echo do_shortcode('[types field="set-background-image" output="raw"]'); } else { echo $template_url.'/images/imageNotAvailable.png;background-size: 100% auto;'; } ?>);">
    <div class="container">
        <div class="row align-items-center justify-content-center">    
            <div class="p-0" data-4500="opacity: 0.5;" data-4900="opacity: 1;" data-5400="opacity: 0.5;">
                <a href="<?php if ( get_post_meta($post->ID, $link, true)) { echo do_shortcode('[types field="link-to-page" output="raw"]'); } else { echo "#"; } ?>">
                    <h3>
                        <?php
                            if ( get_post_meta($post->ID, $gitHeading."-".$lang, true)) echo do_shortcode('[types field="'.ltrim($gitHeading,"wpcf-")."-".$lang.'"]');
                            if ( get_post_meta($post->ID, $gitSubHeading."-".$lang, true)) echo "<span>" . do_shortcode('[types field="'.ltrim($gitSubHeading,"wpcf-")."-".$lang.'"]') . "</span>" ;
                        ?>
                    </h3>

                    <h2>
                        <?php
                            if ( get_post_meta($post->ID, $addheading."-".$lang, true)) echo  do_shortcode('[types field="'.ltrim($addheading,"wpcf-")."-".$lang.'"]');
                        ?> &raquo;
                    </h2>
                </a>
            </div>
        </div>
    </div>
</section>

<?php 
        endwhile; wp_reset_query(); 
    endif;
?>

<?php endwhile; wp_reset_query(); ?>


<?php get_footer(); ?>